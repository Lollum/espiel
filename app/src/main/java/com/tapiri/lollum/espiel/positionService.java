package com.tapiri.lollum.espiel;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import android.hardware.SensorManager;


///////////////////////////////////////////////////
/*
import org.openintents.sensorsimulator.hardware.Sensor;
import org.openintents.sensorsimulator.hardware.SensorEvent;
import org.openintents.sensorsimulator.hardware.SensorEventListener;
import org.openintents.sensorsimulator.hardware.SensorManagerSimulator;
*/
///////////////////////////////////////////////////


import android.location.GpsStatus;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;


import android.content.Context;

import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/*
variazion sui 3 metri: 0.35-0.42
Ottiamale sui 3 metri: 0.37-0.38
errore 0.0099
pressione bologna piano -1:
    1002.7-4
pressione bologna piano 0:
    1002.2-0
pressione bologna piano 1:
    1001.8-6
pressione bologna piano 2:
    1001.3-1
prendo 5 valori media
prendo altri 5 valori media
controllo che sia crescente (scendo) o decrescente (salgo)
*/

public class positionService extends Service implements LocationListener,GpsStatus.Listener {
    private static final int SAMPLING_RATE = 5;
    private static final int TREND_RATE = 7;
    static final int NO_COORDINATE = -500;

    boolean isGPSEnabled = false;
    boolean canGetLocation = false;

    private static final int BARO_SENSOR = 1;
    private static final int PROXY_SENSOR = 0;

    static final int LPS_PURE = 0;
    static final int GPS_FIX = 1;
    static final int LPS_FIX = 2;
    private boolean tempFix = false;

    int altitude = 0;

    private boolean gpsEnabled = false;
    private boolean gpsFix = false;
    private boolean gpsFixOld = false;
    private boolean firstFix = false;
    private boolean firstPressure = true;

    private long locationTime = 0;
    private long elevationTime = 0;
    private Location lastLocation;
    private int googleElevation = 0;

    private static final int MIN_DISTANCE_UPDATES = 0; // sempre attiva

    private static final int MS_TO_SECOND = 1000;
    private static final int MIN_TIME_UPDATES =  5*MS_TO_SECOND;

    private static final int SECOND_TO_MINUTE = 60;
    private static final int EXPIRE_TIME = MS_TO_SECOND*SECOND_TO_MINUTE*5;

    private int deltaExpire = 0;
    //TimerTask increaseDeltaExpire;
    //Timer expireTimer;
    private long expireDate = 0;

    private int gpsTimeout = 10;
    private int elevationPolling = 30*MS_TO_SECOND;
    private double deltaPressure = -5;
    private int altitudeToDeltaPressure = 3;
    private int gpsTime = MIN_TIME_UPDATES;
    private int gpsDistance = MIN_DISTANCE_UPDATES;


    private sensorHandler sensor;

    private double basePressure;
    private double actualPressure;

    int sensorType = 1;
    int activeSensor = 1;

    private boolean sensorActive = false;

    protected LocationManager locationManager;

    int pressureSample=0;
    List<Double> pressionValues = new ArrayList<>();
    List<Integer> altitudeTrends = new ArrayList<>();

    Handler expireHandler = new Handler();

    private String filterSettings = "settings";
    private String filterUpdate = "wantupdate";
    private String filterNewData = "newdata";
    //private String filterNewData = "coordinate";

    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;
    //se devo aggiornare le impostazioni del servizio
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("updateSettings","ricevo aggiornamento");
            if (intent.getAction().equals(filterSettings)){
                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                try {
                    gpsTimeout = Integer.parseInt(SP.getString("gpsTimeout", "10"));
                    elevationPolling = Integer.parseInt(SP.getString("elevationTime", "30"))*MS_TO_SECOND;
                    deltaPressure = Double.parseDouble(SP.getString("deltaPressure", "-5"));
                    sensorType = Integer.parseInt(SP.getString("sensorType", "1"));
                    altitudeToDeltaPressure = Integer.parseInt(SP.getString("deltaAltitude", "3"));
                    gpsTime = Integer.parseInt(SP.getString("gpsTime", "5"))*MS_TO_SECOND;
                    gpsDistance = Integer.parseInt(SP.getString("gpsDistance", String.valueOf(MIN_DISTANCE_UPDATES)));
                } catch(NumberFormatException e) {
                    Log.e("NumberFormatException ", e.toString());
                }
                if (activeSensor != sensorType)
                    sensor.changeSensor(sensorType);
                changeGPSSettings();
            }else{
                if (intent.getAction().equals(filterUpdate)){
                    //Log.d("GOOGLE_TIME", "timeout set:" + (SystemClock.elapsedRealtime()- elevationTime) +" : "+!( SystemClock.elapsedRealtime() - elevationTime < elevationPolling));
                    if (!( SystemClock.elapsedRealtime() - elevationTime < elevationPolling)){
                        if (lastLocation != null){
                            new getElevationFromGoogle().execute(lastLocation);
                            elevationTime = SystemClock.elapsedRealtime();
                        }
                    }
                    //sendMessageUpdate(gpsFix);
                    updateProvider(gpsFix);
                }
            }
        }
    };
    @Override
    public void onCreate(){
        super.onCreate();

        /* *********** SOLO PER IL SIMULATORE, RIMUOVERE IMMEDIATAMENTE PER TEST REALI */
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);
        /********************************************************************************/

        //vado a leggere i parametri dalle impostazioni ed inizializzo sensori e gps
        IntentFilter filter = new IntentFilter(filterSettings);
        filter.addAction(filterUpdate);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        try {
            gpsTimeout = Integer.parseInt(SP.getString("gpsTimeout", "10"));
            elevationPolling = Integer.parseInt(SP.getString("elevationTime", "30"))*MS_TO_SECOND;
            deltaPressure = Double.parseDouble(SP.getString("deltaPressure", "-5"));
            sensorType = Integer.parseInt(SP.getString("sensorType", "1"));
            altitudeToDeltaPressure = Integer.parseInt(SP.getString("deltaAltitude", "3"));
            gpsTime = Integer.parseInt(SP.getString("gpsTime", "5"))*MS_TO_SECOND;
            gpsDistance = Integer.parseInt(SP.getString("gpsDistance", "1"));
        } catch(NumberFormatException e) {
            Log.e("NumberFormatException ", e.toString());
        }
        sensor = new sensorHandler();


        //Log.d("GPS_init", "timeout set:" + gpsTimeout + " | " + elevationPolling + "sensorActive: " + "sensortype: " + sensorType);
        startUsingGPS();
        //Log.d("GOOGLE_TIME", "timeout set:" + (SystemClock.elapsedRealtime()- elevationTime) +" : "+!( SystemClock.elapsedRealtime() - elevationTime < elevationPolling));

        //dato che il numero di richieste a google sono limitate, evito di tempestare il servizio di richieste limitandomi al massimo ad una chiamata ogni elevationPolling secondi
        if (!( SystemClock.elapsedRealtime() - elevationTime < elevationPolling)){
            if (lastLocation != null){
                new getElevationFromGoogle().execute(lastLocation);
                elevationTime = SystemClock.elapsedRealtime();
            }
        }

        //sendMessageUpdate(gpsFix);
        updateProvider(gpsFix);
        //LocalBroadcastManager.getInstance(this).registerReceiver(updateReceiver, new IntentFilter("wantupdate"));
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "EspielPowerLock");
        wakeLock.acquire();
        expireDate = SystemClock.elapsedRealtime();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d("GPS_destroy", "Spengo gps e sensori");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(updateReceiver);
        stopUsingGPS();
        sensor.stopSensorHandler();
        wakeLock.release();
    }

    //inizializzo il gps e e prendo l'ultima locazione conosciuta
    public Location startUsingGPS() {
        try {
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            locationManager.addGpsStatusListener(this);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                this.canGetLocation = true;
                if (lastLocation == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, gpsTime, gpsDistance, this);
                    if (locationManager != null) {
                        lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                   }
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
        //Log.d("GPS_STARTUSING", "Impostazioni Iniziali; Tempo: " + gpsTime +" Distanza:"+ gpsDistance);
        //Log.d("GPS_STARTUSING", "Lat: " + lastLocation.getLatitude() + " Long: " + lastLocation.getLongitude());
        return lastLocation;
    }

    //se vengono cambiate le impostazioni mentre il servizio e' attivo chiamo questa funzione per effettuare i cambiamenti
    public void changeGPSSettings(){
        stopUsingGPS();
        try {
            locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
            locationManager.addGpsStatusListener(this);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                this.canGetLocation = true;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, gpsTime, gpsDistance, this);
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
        Log.d("GPS_SETTINGS", "Nuove Impostazioni; Tempo: " + gpsTime +" Distanza:"+ gpsDistance);
    }

    //smetto di usare il servizio gps
    public void stopUsingGPS(){
        Log.d("GPS_destroy", "Permission " + (checkCallingPermission("android.permission.ACCESS_FINE_LOCATION")));
        if((locationManager != null) /*&& (checkCallingPermission("android.permission.ACCESS_FINE_LOCATION")== PackageManager.PERMISSION_GRANTED)*/){
            locationManager.removeGpsStatusListener(positionService.this);
            locationManager.removeUpdates(positionService.this);
            locationManager = null;
        }
    }

    //quando il gps riceve una nuova locazione, aggiorno i dati
    @Override
    public void onLocationChanged(Location newlocation) {
        Log.d("GPS", "Nuova Location");
        gpsFix = true;
        lastLocation = newlocation;
        locationTime = SystemClock.elapsedRealtime();
        if (!( SystemClock.elapsedRealtime() - elevationTime < elevationPolling)){
            new getElevationFromGoogle().execute(lastLocation);
            elevationTime = SystemClock.elapsedRealtime();
        }

        //////////////////////////////////////////////////////////////////////////////////////////

        if (Math.abs(getAltitudeVariation(actualPressure))<= deltaExpire || firstPressure){
            firstPressure = false;
            tempFix= false;
            basePressure = actualPressure;
            deltaExpire = 0;
            expireDate=SystemClock.elapsedRealtime();
        }else{
            tempFix = true;
        }

        //////////////////////////////////////////////////////////////////////////////////////////

        //basePressure = sensor.getPressure();
        //sendMessageUpdate(gpsFix);
        updateProvider(gpsFix);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    /*********************************************************************************************/
    private void updateProvider(boolean fixGPS){
        ContentValues values_gps = new ContentValues();
        int gpsState = LPS_PURE;
        if (fixGPS){
            if (tempFix){
                gpsState = LPS_FIX;
            }else{
                gpsState = GPS_FIX;
            }

        }
        if (lastLocation != null){
            values_gps.put(EspielProvider.LATITUDE, lastLocation.getLatitude());
            values_gps.put(EspielProvider.LONGITUDE, lastLocation.getLongitude());
            values_gps.put(EspielProvider.GPSACCURACY,lastLocation.getAccuracy());
        }else{
            values_gps.put(EspielProvider.LATITUDE,NO_COORDINATE);
            values_gps.put(EspielProvider.LONGITUDE,NO_COORDINATE);
            values_gps.put(EspielProvider.GPSACCURACY,0);
        }
        values_gps.put(EspielProvider.ELEVATION,googleElevation);
        values_gps.put(EspielProvider.GPSFIX,gpsState);

        ContentValues values_sensor = new ContentValues();
        values_sensor.put(EspielProvider.READED_BASEPRESSURE,basePressure);
        values_sensor.put(EspielProvider.READED_PRESSURE,actualPressure);
        values_sensor.put(EspielProvider.READED_ALTITUDE,altitude);
        values_sensor.put(EspielProvider.SENSORACCURACY,sensor.getSensorAccuracy());

        getContentResolver().insert(EspielProvider.GPS_URI, values_gps);
        getContentResolver().insert(EspielProvider.READED_SENSOR_URI, values_sensor);

        Intent intent = new Intent(filterNewData);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    //controllo stato del gps
    @Override
    public void onGpsStatusChanged(int changeType){
        //Log.d("GPS","GpsStatusChanged:"+changeType);
        if (locationManager != null) {
            switch(changeType) {
                //chiamato quando viene effettuato il primo fix
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    Log.d("GPS","FIRST FIX:"+changeType);
                    gpsEnabled = true;
                    gpsFix = true;
                    gpsFixOld = false;
                    firstFix = true;
                    elevationTime = SystemClock.elapsedRealtime();
                    break;
                //chiamato periodicamente, se il tempo passato dall'ultimo fix e' superiore ad un determinato timeout, lo considero perso.
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    /*Log.d("GPS", "UPDATE:" + changeType + "Tempo sistema: " + String.valueOf(SystemClock.elapsedRealtime()) +
                            " tempo:" + String.valueOf(locationTime) + ":" + String.valueOf(SystemClock.elapsedRealtime() - locationTime) +
                            " Gps timeout:" + String.valueOf(gpsTimeout*1000));*/
                    if (lastLocation != null) {
                        Log.d("GPS", "UPDATE:" + changeType + "Tempo passato" + String.valueOf(SystemClock.elapsedRealtime() - locationTime) +
                                "Accuracy:" + lastLocation.getAccuracy());
                        gpsFix = (SystemClock.elapsedRealtime() - locationTime) < gpsTimeout * 1000;
                        gpsFixOld = true;
                    }
                    gpsEnabled = true;

                    break;
                //gps acceso
                case GpsStatus.GPS_EVENT_STARTED:
                    Log.d("GPS","ON:"+changeType);
                    gpsEnabled = true;
                    gpsFix = false;
                    gpsFixOld = false;
                    break;
                //gps spento
                case GpsStatus.GPS_EVENT_STOPPED:
                    Log.d("GPS","OFF:"+changeType);

                    gpsEnabled = false;
                    gpsFix = false;
                    gpsFixOld = false;

                    break;
                default:

                    //Log.d("GPS","Non dovrei esistere");
                    return;
            }
            if (( SystemClock.elapsedRealtime()-expireDate) >= EXPIRE_TIME){
                deltaExpire++;
                Log.d("EXPIRED_TIME","Cambio Diff Orario: "+( SystemClock.elapsedRealtime()-expireDate)+":"+EXPIRE_TIME+" Delta Expire: "+deltaExpire);
                expireDate=SystemClock.elapsedRealtime();
            }else{
                Log.d("EXPIRED_TIME","CAmbio Diff  Orario: "+( SystemClock.elapsedRealtime()-expireDate)+":"+EXPIRE_TIME+" Delta Expire: "+deltaExpire);
            }

            //se ho perso il fix, mando il messaggio relativo al passaggio a LPS
            Log.d("GPS_switch","cosa succede?"+gpsFix+":"+gpsFixOld);
            if (firstFix){
                if (!gpsFix && gpsFixOld){
                    Log.d("GPS_switch", "Non ho fix, tipo:" + changeType);
                    gpsFixOld = false;
                    //sendMessageLocation(NO_COORDINATE,NO_COORDINATE,NO_COORDINATE,gpsFix, 0);
                    updateProvider(gpsFix);
                }else{
                    Log.d("GPS_switch","Ho fix, tipo:"+changeType);
                    gpsFixOld = true;
                }
            }else{
                Log.d("GPS_switch", "Non mai trovato fix, tipo:" + changeType);
            }
        }
    }

    //prendo elevazione dal servizio google
    private class getElevationFromGoogle extends AsyncTask<Location, String, Void> {
        InputStream iStream = null;
        String result = "";

        String textUrl = "https://maps.googleapis.com/maps/api/elevation/json?locations=";

        //chiamo il servizio e prendo il JSON che mi viene restituito
        @Override
        protected Void doInBackground(Location... coordinate) {
            //Log.d("GoogleElevation","doInBackground");
            try {
                textUrl = textUrl+coordinate[0].getLatitude()+","+coordinate[0].getLongitude()+"&key="+getResources().getString(R.string.google_maps_key);
                URL url = new URL(textUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000); // 10 secondi
                conn.setConnectTimeout(15000); //15 secondi
                conn.setDoInput(true); //permette input
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("GoogleElevation", "L'url è:" + textUrl + " The response is: " + response);
                iStream = conn.getInputStream();
                //Log.d("GoogleElevation", "Stream: "+iStream.toString());
            }catch (UnsupportedEncodingException e1) {
                Log.e("UEE", e1.toString());
                e1.printStackTrace();
            } catch (IllegalStateException e2) {
                Log.e("IllegalStateException", e2.toString());
                e2.printStackTrace();
            } catch (IOException e3) {
                Log.e("IOException", e3.toString());
                e3.printStackTrace();
            }
            try {
                //converto il risultato in maniera leggibile
                BufferedReader bReader = new BufferedReader(new InputStreamReader(iStream, "utf-8"), 8);
                StringBuilder sBuilder = new StringBuilder();

                String line; //null
                while ((line = bReader.readLine()) != null) {
                    sBuilder.append(line).append("\n");
                }

                iStream.close();
                Log.d("GoogleElevation", "Stream: " + sBuilder.toString());
                result = sBuilder.toString();

            } catch (Exception e) {
                Log.e("StrBuild", "Error converting " + e.toString());
            }
            //Log.d("GoogleElevation","FinedoInBackground");
            return null;
        }

        //parso il JSON e lo restituisco come variabile
        protected void onPostExecute(Void v) {
            String elevation = "None";
            try {
                JSONObject jObject = new JSONObject(result);
                JSONArray jsonArray = jObject.optJSONArray("results");
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Double jsonElevation = Double.parseDouble(jsonObject.optString("elevation"));
                    googleElevation = jsonElevation.intValue();
                    //sendMessageUpdate(gpsFix);
                    updateProvider(gpsFix);

                }
                Log.d("GoogleElevation","Elevazione:"+elevation);
            } catch (JSONException e) {
                Log.e("JSONException", "Error: " + e.toString());
            }
        }

    }

    //gestore dei sensori
    private class sensorHandler implements SensorEventListener{
        SensorManager sensorManager;
        /******************************/
        //SensorManagerSimulator sensorManager;
        /************************/
        double newPressure = -5;
        boolean start = true;
        double sensorAccuracy = -1;
        Sensor internalSensor;

        //inizializzo il gestore dei sensori
        public sensorHandler(){
            sensorManager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
            /****************SIMULATORE*****************************/
            //sensorManager = SensorManagerSimulator.getSystemService(getBaseContext(), SENSOR_SERVICE);
            //sensorManager.connectSimulator();
            /*******************************************************/
            switchSensor(sensorType);
        }


        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            sensorAccuracy = sensor.getResolution();
            //sensorAccuracy = 0;
        }

        //quando rilevo un cambiamento del valore del sensore effettuo calcoli di conseguenza
        @Override
        public void onSensorChanged(SensorEvent event) {
            float[] values = event.values;
            newPressure = values[0];//*10000;// simulatore
            sensorAccuracy = event.sensor.getResolution();
            //sensorAccuracy = 0;
            //in base al tipo di sensore cambio la logica
            switch (sensorType){
                case PROXY_SENSOR:
                    if (start){
                        Log.d("SENSOR", "////Imposto Pressione Base" + newPressure);
                        basePressure = newPressure;
                        start = false;
                    }
                    actualPressure=newPressure;
                    if (actualPressure < basePressure){
                        //Log.d("SENSOR", "++++++++++++++++");
                        altitude = (int)((((basePressure - actualPressure))/deltaPressure)*altitudeToDeltaPressure);
                    }else{
                        //Log.d("SENSOR", "----------------");
                        altitude = (int)((((actualPressure - basePressure))/deltaPressure)*-altitudeToDeltaPressure);
                    }
                    //sendMessageUpdate(gpsFix);
                    updateProvider(gpsFix);
                    break;
                case BARO_SENSOR:
                    pressionValues.add(newPressure);
                    if (start){
                        Log.d("SENSOR", "////Imposto Pressione Base" + newPressure);
                        basePressure = averagePressure(pressionValues);//newPressure;
                        start = false;
                    }
                    Log.d("SENSOR", "****Sensore:" + newPressure + "Pressione Base" + basePressure + "Pressione attuale" + actualPressure);
                    //Log.d("averagePressure", "PressureSample:"+(pressureSample%5));
                    if (pressureSample==SAMPLING_RATE){
                        //Log.d("averagePressure", "Valori al 5:"+pressionValues+" | "+pressureSample);
                        pressureSample=0;
                        actualPressure = averagePressure(pressionValues);
                        getAltitudeVariation(actualPressure);
                        //sendMessageUpdate(gpsFix);
                        updateProvider(gpsFix);
                        pressionValues.clear();
                    }else{
                        //Log.d("averagePressure", "Valori non 5:"+pressionValues+" | "+pressureSample);
                        pressureSample++;
                    }
                    break;
                default:
                    break;

            }
        }

        public double getPressure(){
            return newPressure;
        }
        public double getSensorAccuracy(){
            return sensorAccuracy;
        }

        //fermoi il gestore dei sensori
        public void stopSensorHandler(){
            if (internalSensor != null)
                sensorManager.unregisterListener(this,internalSensor);
            start = true;
            /**********************************/
            //sensorManager.disconnectSimulator();
            /**********************************/
        }

        //cambio il sensore in base alle impostazioni
        private void switchSensor(int newSensor){
            switch (newSensor){
                case PROXY_SENSOR:
                    internalSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
                    break;
                case BARO_SENSOR:
                    internalSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
                    break;
                default:
                    internalSensor = null;
                    break;

            }
            if (internalSensor != null) {
                sensorManager.registerListener(this, internalSensor, SensorManager.SENSOR_DELAY_UI);
                sensorActive = true;
            }
            else{
                sensorActive = false;
            }
            activeSensor = newSensor;
        }

        public void changeSensor(int newSensor){
            stopSensorHandler();
            switchSensor(newSensor);
        }
    }

    //calcolo la variazione di altitudine in base a quale sensore ho utilizzato
    private double getAltitudeVariation(double instantPressure){
        //Log.d("SENSOR_VALUE", "basePressure" + basePressure +" instantPressure" + instantPressure);
        //Log.d("SENSOR_ACCURACY","accuracy: "+ sensor.getSensorAccuracy() + "Variazione: "+ (instantPressure - basePressure));
        //Log.d("SENSOR", "delta positivo " + ((basePressure - instantPressure)+ sensor.getAccuracy())+" delta negativo " + ((instantPressure - basePressure)+ sensor.getAccuracy()));
        switch(sensorType){
            case PROXY_SENSOR:
                if ((instantPressure - basePressure) == 0){
                    return altitude = 0;
                }
                altitude = (int)((instantPressure - basePressure) *altitudeToDeltaPressure);
                break;
            case BARO_SENSOR:
                /*
                if (((instantPressure - basePressure) <= sensor.getSensorAccuracy()) && ((basePressure - instantPressure) <= sensor.getSensorAccuracy()) ){
                    return altitude = 0;
                }
                */
                altitude = (int)SensorManager.getAltitude((float)basePressure,(float)instantPressure);
                //Log.d("SENSOR_altitude", "Valori pressione:"+basePressure+" | "+instantPressure + "|"+ altitude);
                altitude = getTrendAltitude(altitude);
                break;
            default:
                break;
        }

        //Log.d("SENSOR_altitude", "Valori pressione:"+basePressure+" | "+instantPressure + "|"+ altitude);
        return altitude;
    }

    //stabilizzo dati ricevuto dal sensore di pressione
    private double averagePressure(List<Double> values/*double[] values*/){
        double average=0;
        int elements=0;
        for (double pressure:values){
            average+=pressure;
            elements++;
        }
        //Log.d("averagePressure", "Valori:"+average+" | "+elements);
        return average/elements;
    }

    //prendo la moda dell'attuale altitudine, per eliminare anomalie temporanee
    private int getTrendAltitude(int newAltitude){

        int possibleValue = 0;
        int possibleTrend = 0;

        int currentValue = 0;
        int currentTrend = 0;
        altitudeTrends.add(newAltitude);
        if (altitudeTrends.size() == TREND_RATE){
            List<Integer> bufferTrends = new ArrayList<>(altitudeTrends);
            Collections.sort(bufferTrends);
            for (int value:bufferTrends){
                if (value == currentValue){
                    currentTrend++;
                }else{
                    if (possibleValue == value){
                        possibleTrend++;
                    }else{
                        possibleValue=value;
                        possibleTrend=1;
                    }
                }
                if (possibleTrend >= currentTrend){
                    currentValue = possibleValue;
                    currentTrend = possibleTrend;
                }
            }
            Log.d("AVERAGE_trend", "Valori trend:"+altitudeTrends+" trend attuale: "+currentValue+"Trend:"+possibleValue+":"+possibleTrend+" | "+currentValue+":"+currentTrend);
            altitudeTrends.remove(0);
        }else{
            Log.d("AVERAGE_trend", "Valori trend:"+altitudeTrends+" trend attuale: "+currentValue);
            currentValue = newAltitude;
        }

        return currentValue;
    }


}