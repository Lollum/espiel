package com.tapiri.lollum.espiel;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;

import java.util.Date;
import java.util.HashMap;

public class EspielProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.tapiri.lollum.provider.EspielData";
    static final String GPS_URL = "content://" + PROVIDER_NAME + "/gps";
    static final Uri GPS_URI = Uri.parse(GPS_URL);

    static final String READED_SENSOR_URL = "content://" + PROVIDER_NAME + "/readedsensor";
    static final Uri READED_SENSOR_URI = Uri.parse(READED_SENSOR_URL);

    static final String USABLE_SENSOR_URL = "content://" + PROVIDER_NAME + "/usablesensor";
    static final Uri USABLE_SENSOR_URI = Uri.parse(USABLE_SENSOR_URL);

    static final String PLUS_URL = "content://" + PROVIDER_NAME + "/plussensor";
    static final Uri PLUS_URI = Uri.parse(PLUS_URL);

    static final int GPS = 0;
    static final int READED_SENSOR = 1;
    static final int USABLE_SENSOR = 2;
    static final int PLUS_SENSOR = 3;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "gps", GPS);
        uriMatcher.addURI(PROVIDER_NAME, "readedsensor", READED_SENSOR);
        uriMatcher.addURI(PROVIDER_NAME, "usablesensor", USABLE_SENSOR);
        uriMatcher.addURI(PROVIDER_NAME, "plussensor", PLUS_SENSOR);
    }

    /************ GPS *************/
    private int elevation = -1;
    static final String ELEVATION = "elevation";
    private double lastLatitude = positionService.NO_COORDINATE;
    static final String LATITUDE = "latitude";
    private double lastLongitude = positionService.NO_COORDINATE;
    static final String LONGITUDE = "longitude";
    private double gpsFix = 0;
    static final String GPSFIX = "gpsfix";
    private double gpsAccuracy = positionService.NO_COORDINATE;
    static final String GPSACCURACY = "gpsaccuracy";
    /******** SENSOR LETTI *********/
    private double readedBasePressure = -1.0;//pressione base usata
    static final String READED_BASEPRESSURE = "readedbasepressure";
    private int readedAltitude = -1; //altitudine usata
    static final String READED_ALTITUDE = "readedaltitude";
    private double readedPressure = -1.0; //pressione istantanea
    static final String READED_PRESSURE = "readedpressure";
    /******** SENSOR PUBBLICATI *********/
    private double usedBasePressure = -1.0;//pressione base usata
    static final String BASEPRESSURE = "usedbasepressure";
    private int usedAltitude= -1; //altitudine usata
    static final String ALTITUDE = "usedaltitude";
    private double usedPressure= -1.0; //pressione istantanea
    static final String PRESSURE = "instantpressure";
    private double sensorAccuracy= -1.0; //pressione istantanea
    static final String SENSORACCURACY = "sensoraccuracy";
    private double guessValue = 2; //pressione istantanea
    static final String GUESS_VALUE = "guessvalue";
    /******** ADDIZIONALI *********/
    private int pureAltitude = -1; //altitudine calcolata dalla pressione base
    static final String PURE_ALTITUDE = "purealtitude";
    private int correctedAltitude = -1; //altitudine corretta con algoritmo
    static final String CORRECTED_ALTITUDE = "correctedaltitude";
    private double internalBasePressure = -1.0; //pressione base rilevata dal dispositivo
    static final String INTERNAL_BASEPRESSURE = "internalbasepressure";
    private double externalBasePressure = -1.0; //pressione base da altri dispositivi
    static final String EXTERNAL_BASEPRESSURE = "externalbasepressure";
    private int offsetComplete = 0; //pressione base da altri dispositivi
    static final String OFFSETCOMPLETE = "offsetcomplete";
    private int offsetLocal = 0; //pressione base da altri dispositivi
    static final String OFFSETLOCAL = "offsetlocal";

    @Override
    public boolean onCreate() {

        return true;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)){
            case GPS:
                elevation = values.getAsInteger(ELEVATION);
                lastLatitude = values.getAsDouble(LATITUDE);
                lastLongitude = values.getAsDouble(LONGITUDE);
                gpsFix = values.getAsInteger(GPSFIX);
                gpsAccuracy = values.getAsDouble(GPSACCURACY);
                Log.d("TestProviderGPS","elevation"+elevation+"lastLatitude"+lastLatitude+
                        "lastLongitude"+lastLongitude+"gpsFix"+gpsFix+ "gpsAccuracy"+gpsAccuracy);
                break;
            case READED_SENSOR:
                readedBasePressure = values.getAsDouble(READED_BASEPRESSURE);
                readedAltitude = values.getAsInteger(READED_ALTITUDE);
                readedPressure = values.getAsDouble(READED_PRESSURE);
                sensorAccuracy = values.getAsDouble(SENSORACCURACY);
                Log.d("TestProviderReaded","readedBasePressure"+readedBasePressure+"readedAltitude"+
                        readedAltitude+ "readedPressure"+readedPressure+"sensorAccuracy"+sensorAccuracy);
                break;
            case USABLE_SENSOR:
                usedBasePressure = values.getAsDouble(BASEPRESSURE);
                usedAltitude = values.getAsInteger(ALTITUDE);
                usedPressure = values.getAsDouble(PRESSURE);
                guessValue = values.getAsInteger(GUESS_VALUE);
                Log.d("TestProviderUsable","usedBasePressure"+usedBasePressure+"usedAltitude"+
                        usedAltitude+"usedPressure"+usedPressure+"guessValue"+guessValue);
                break;
            case PLUS_SENSOR:
                pureAltitude = values.getAsInteger(PURE_ALTITUDE);
                correctedAltitude = values.getAsInteger(CORRECTED_ALTITUDE);
                internalBasePressure = values.getAsDouble(INTERNAL_BASEPRESSURE);
                externalBasePressure = values.getAsDouble(EXTERNAL_BASEPRESSURE);
                offsetComplete = values.getAsInteger(OFFSETCOMPLETE);
                offsetLocal = values.getAsInteger(OFFSETLOCAL);
                Log.d("TestProviderPlus","pureAltitude"+pureAltitude+"correctedAltitude"+correctedAltitude+
                        "internalBasePressure"+internalBasePressure+"externalBasePressure"+externalBasePressure+
                        "offsetComplete"+offsetComplete+"offsetLocal"+offsetLocal);
                break;
            default:
                // errore
                return uri;
        }
        return uri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder) {
        MatrixCursor valuesCursor;
        switch (uriMatcher.match(uri)){
            case GPS:
                String[] gps_values = {ELEVATION,LATITUDE,LONGITUDE,GPSFIX,GPSACCURACY};
                valuesCursor = new MatrixCursor(gps_values);
                valuesCursor.addRow(new Object[]{elevation, lastLatitude, lastLongitude, gpsFix, gpsAccuracy});
                break;
            case READED_SENSOR:
                String[] rsensor_values= {READED_BASEPRESSURE,READED_PRESSURE,READED_ALTITUDE};
                valuesCursor = new MatrixCursor(rsensor_values);
                valuesCursor.addRow(new Object[]{readedBasePressure, readedPressure, readedAltitude});
                break;
            case USABLE_SENSOR:
                String[] usensor_values= {BASEPRESSURE,PRESSURE,ALTITUDE,GUESS_VALUE,SENSORACCURACY};
                valuesCursor = new MatrixCursor(usensor_values);
                valuesCursor.addRow(new Object[]{usedBasePressure, usedPressure, usedAltitude,guessValue,sensorAccuracy});
                break;
            case PLUS_SENSOR:
                String[] psensor_values= {INTERNAL_BASEPRESSURE,EXTERNAL_BASEPRESSURE,PURE_ALTITUDE,CORRECTED_ALTITUDE,OFFSETCOMPLETE,OFFSETLOCAL};
                valuesCursor = new MatrixCursor(psensor_values);
                valuesCursor.addRow(new Object[]{internalBasePressure,externalBasePressure,pureAltitude,correctedAltitude,offsetComplete,offsetLocal});
                break;
            default:
                // errore
                String[] error_values= {};
                valuesCursor = new MatrixCursor(error_values);
        }
        return valuesCursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (uriMatcher.match(uri)){
            case GPS:
                elevation = values.getAsInteger(ELEVATION);
                lastLatitude = values.getAsDouble(LATITUDE);
                lastLongitude = values.getAsDouble(LONGITUDE);
                gpsFix = values.getAsInteger(GPSFIX);
                gpsAccuracy = values.getAsDouble(GPSACCURACY);
                break;
            case READED_SENSOR:
                readedBasePressure = values.getAsInteger(READED_BASEPRESSURE);
                readedAltitude = values.getAsInteger(READED_ALTITUDE);
                readedPressure = values.getAsDouble(READED_PRESSURE);
                sensorAccuracy = values.getAsDouble(SENSORACCURACY);
                break;
            case USABLE_SENSOR:
                usedBasePressure = values.getAsInteger(BASEPRESSURE);
                usedAltitude = values.getAsInteger(ALTITUDE);
                usedPressure = values.getAsDouble(PRESSURE);
                guessValue = values.getAsInteger(GUESS_VALUE);
                break;
            case PLUS_SENSOR:
                pureAltitude = values.getAsInteger(PURE_ALTITUDE);
                correctedAltitude = values.getAsInteger(CORRECTED_ALTITUDE);
                internalBasePressure = values.getAsDouble(INTERNAL_BASEPRESSURE);
                externalBasePressure = values.getAsDouble(EXTERNAL_BASEPRESSURE);
                offsetComplete = values.getAsInteger(OFFSETCOMPLETE);
                offsetLocal = values.getAsInteger(OFFSETLOCAL);
                break;
            default:
                // errore
                return 1;
        }
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case GPS:
                return "vnd.android.cursor.item/com.tapiri.lollum.gps";
            case READED_SENSOR:
                return "vnd.android.cursor.item/com.tapiri.lollum.readedsensor";
            case USABLE_SENSOR:
                return "vnd.android.cursor.itemcom.tapiri.lollum.usablesensor";
            case PLUS_SENSOR:
                return "vnd.android.cursor.item/com.tapiri.lollum.plussensor";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
}
