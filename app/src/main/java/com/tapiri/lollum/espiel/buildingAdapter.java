package com.tapiri.lollum.espiel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class buildingAdapter extends ArrayAdapter<File> {
    private final Context context;
    private final List<File> values;

    public buildingAdapter(Context context, List<File> values) {
        super(context, R.layout.building_adapter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String date;
        String time;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.building_adapter, parent, false);
        }
        TextView textName = (TextView) convertView.findViewById(R.id.name);
        TextView textAddress = (TextView) convertView.findViewById(R.id.address);
        TextView textDate = (TextView) convertView.findViewById(R.id.date);
        String[] currentline = values.get(position).getName().split("_");
        date = currentline[0].substring(0,4)+"-"+currentline[0].substring(4,6)+"-"+currentline[0].substring(6,8);
        time = currentline[1].substring(0, 2)+":"+currentline[0].substring(2,4);
        textName.setText(currentline[2].replace("-"," "));
        textAddress.setText(currentline[3].replace("-"," "));
        textDate.setText(date+" Ore:"+time);
        return convertView;
    }
} 

