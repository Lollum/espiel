package com.tapiri.lollum.espiel;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeCallback;
import com.google.android.gms.nearby.messages.SubscribeOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

//Servizio Moire:
//Cloto: controlla passato basandosi su input precedenti
//Lachesi: stima il trend attuale e contatta dispositivi adiacenti per confermare
//Atropo: calcola il valore finale basandosi sui dati dei precedenti

public class fateService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final int MS_TO_SECOND = 1000;
    int delay = 60*MS_TO_SECOND; //milliseconds

    int bfrFix;
    float bfrAccuracy;
    double bfrLatitude;
    double bfrLongitude;
    int bfrAltitude;
    int bfrElevation;
    double bfrActualPressure;
    double bfrBasePressure;
    double bfrGuessedBasePressure;
    int bfrGuess; //0 fermo 1 in movimento 2 troppo presto 3 GPS
    private String filterProData = "prodata";
    private String filterNewData = "newdata";
    private String filterSettings = "settings";

    private int maxPressureSamples = 25;
    private int maxGuessSamples = 3;

    private boolean noFix = true;

    private double pressureTotal = 0;
    private ArrayList<Double> pastPressures;
    private ArrayList<Boolean> pastConditions;
    private ArrayList<Boolean> pastGuess;
    private double maxPressure = 0;
    private double minPressure = 5000;

    private double actualPressure = 0;
    private double lastPressureValue = 0;

    private Date lastPressureDate;
    private SimpleDateFormat timeFormat;
    private boolean firstPressure = true;
    private boolean startGuessing = false;
    private int guessingCount = 0;
    private int movementError = 5;
    private double stimaDiv = 1.9;

    private int baseAltitudeDelta = 0;
    private int guessedAltitudeDelta = 0;

    private boolean haveDevicesValues=false;
    private double externalValue = 0.0;

    private int altitudeMoving = 0;
    private int altitudeStationary = 0;
    private int altitudeOffset = 0;
    private int altitudeMovOffset = 0;
    private int altitudeStayOffset = 0;
    private int altitudeMovingInternal = 0;
    private int altitudeStationaryInternal  = 0;
    private int altitudeOffsetInternal  = 0;
    private int altitudeMovOffsetInternal  = 0;
    private int altitudeStayOffsetInternal  = 0;
    Handler timeHandler = new Handler();

    private boolean restartService = false;
    private int hasRelay = 0;
    /***************************************/
    private static final String TAG = "nearby";
    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder().setTtlSeconds(30).build(); // in secondi
    private final ArrayList<String> mNearbyDevicesArrayList = new ArrayList<>();
    private final ArrayList<pressureMessage> mPressureMessageList = new ArrayList<>();
    private GoogleApiClient mGoogleApiClient;
    private Message mDeviceInfoMessage;
    private MessageListener mMessageListener;
    private boolean mResolvingNearbyPermissionError = false;


    private boolean firstPublish = true;
    /**************************************/

    private int useBeacon = 0;

    SimpleDateFormat timeFormatFile = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("moire", "ricevo:");
            if (intent.getAction().equals(filterNewData)){
                readFromProvider();
                if (noFix){
                    lastPressureDate = Calendar.getInstance().getTime();
                    haveDevicesValues = false;
                    noFix = false;
                }else{
                    if (bfrFix == 1){
                        lastPressureDate = Calendar.getInstance().getTime();
                        haveDevicesValues = false;
                    }
                }
                if (bfrFix == 1){
                    bfrGuess = 3;
                    estimatePast(bfrActualPressure, bfrAltitude);
                }else{
                    if (haveDevicesValues){
                        int correction;
                        correction = (int)SensorManager.getAltitude((float)externalValue,(float)bfrActualPressure);
                        estimatePast(bfrActualPressure, correction);
                    }else {
                        estimatePast(bfrActualPressure, bfrAltitude);
                    }
                }
                double pressureToSend;
                switch (hasRelay) {
                    case 0:
                        pressureToSend=lastPressureValue;
                        break;
                    case 1:
                        if (haveDevicesValues) {
                            pressureToSend=externalValue;
                        }else{
                            pressureToSend=lastPressureValue;
                        }
                        break;
                    default:
                        pressureToSend=lastPressureValue;
                        break;
                }
                mDeviceInfoMessage = pressureMessage.newNearbyMessage(InstanceID.getInstance(getApplicationContext()).getId(), bfrGuess, pressureToSend, timeFormatFile.format(lastPressureDate));
                //sendMessageLocation();
                updateProvider();
            }else if (intent.getAction().equals(filterSettings)){
                restartService = true;
                stopService(new Intent(getApplicationContext(), fateService.class));
                Log.d("estimateSetting","riavvio servizio");
            }
        }
    };

    private void readFromProvider(){
        String URL_GPS = EspielProvider.GPS_URL;
        String URL_SENSOR = EspielProvider.READED_SENSOR_URL;

        Uri gps_data = Uri.parse(URL_GPS);
        Uri sensor_data = Uri.parse(URL_SENSOR);

        Cursor c_gps = getApplicationContext().getContentResolver().query(gps_data, null, null, null, null);
        Cursor c_sensor = getApplicationContext().getContentResolver().query(sensor_data,null,null,null,null);
        if (c_gps != null && c_gps.moveToFirst()){
            bfrElevation = c_gps.getInt(0);
            bfrLatitude = c_gps.getDouble(1);
            bfrLongitude = c_gps.getDouble(2);
            bfrFix = c_gps.getInt(3);
            bfrAccuracy = c_gps.getFloat(4);
            c_gps.close();
        }
        if (c_sensor != null && c_sensor.moveToFirst()){
            bfrAltitude = c_sensor.getInt(2);
            bfrActualPressure = c_sensor.getDouble(1);
            lastPressureValue = bfrBasePressure = c_sensor.getDouble(0);
            c_sensor.close();
        }
        Log.d("TestProviderFate","elevation:" + bfrElevation+" lat:"+bfrLatitude+" long:"+bfrLongitude+"bfrFix: "+bfrFix+
                " bfrAltitude:" + bfrAltitude+" bfrActualPressure:"+bfrActualPressure+" lastPressureValue:"+lastPressureValue);
    }

    private void updateProvider(){
        ContentValues values_sensor = new ContentValues();
        if (bfrFix == 1) {
            altitudeStationary = altitudeMoving = guessedAltitudeDelta = bfrAltitude;
            haveDevicesValues = false;
        }
        if (haveDevicesValues) {
            int correction;
            correction = (int) SensorManager.getAltitude((float) externalValue, (float) bfrActualPressure);
            values_sensor.put(EspielProvider.ALTITUDE, correction + altitudeOffset);
        } else {
            values_sensor.put(EspielProvider.ALTITUDE,guessedAltitudeDelta );
        }
        values_sensor.put(EspielProvider.BASEPRESSURE,bfrGuessedBasePressure);
        values_sensor.put(EspielProvider.PRESSURE,bfrActualPressure);
        values_sensor.put(EspielProvider.GUESS_VALUE,bfrGuess);

        ContentValues values_plus = new ContentValues();
        values_plus.put(EspielProvider.EXTERNAL_BASEPRESSURE,externalValue);
        values_plus.put(EspielProvider.PURE_ALTITUDE,bfrAltitude);
        values_plus.put(EspielProvider.OFFSETCOMPLETE,altitudeOffset);
        values_plus.put(EspielProvider.INTERNAL_BASEPRESSURE,lastPressureValue);
        values_plus.put(EspielProvider.CORRECTED_ALTITUDE,bfrAltitude+altitudeOffsetInternal);
        values_plus.put(EspielProvider.OFFSETLOCAL,altitudeOffsetInternal);

        getContentResolver().insert(EspielProvider.USABLE_SENSOR_URI, values_sensor);
        getContentResolver().insert(EspielProvider.PLUS_URI, values_plus);

        Intent intent = new Intent(filterProData);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void estimatePast(double usedPressure, int usedAltitude){
        double olderPressure = 0;
        double medianPressure;
        double deltaMedianActual;
        double deltaMaxMin;
        double stima;
        boolean tempGuess = true;
        if (pastPressures.size() == maxPressureSamples ){
            olderPressure = pastPressures.remove(0);
            pastPressures.add(usedPressure);
            startGuessing = true;
        }else{
            pastPressures.add(usedPressure);
        }
        if (olderPressure == minPressure || olderPressure == maxPressure){
            findNewMinMax();
        }
        if (firstPressure){
            maxPressure = usedPressure;
            minPressure = usedPressure;
            firstPressure = false;
        }else{
            if (maxPressure < usedPressure){
                maxPressure = usedPressure;
            }
            if (minPressure > usedPressure){
                minPressure = usedPressure;
            }
        }
        deltaMaxMin = maxPressure-minPressure;
        pressureTotal = pressureTotal - olderPressure + usedPressure;
        medianPressure = (pressureTotal/pastPressures.size());
        if (usedPressure >= medianPressure){
            deltaMedianActual = usedPressure-medianPressure;
        }else{
            deltaMedianActual = medianPressure-usedPressure;
        }
        stima = ((deltaMaxMin)/stimaDiv);
        if (pastConditions.size() == maxPressureSamples ){
            pastConditions.remove(0);
            pastConditions.add(stima>deltaMedianActual);
        }else{
            pastConditions.add(stima>deltaMedianActual);
        }
        /***********inizio indovino****************/
        if (bfrFix != 1){
            tempGuess = checkPastCondition();
            if (startGuessing){
                guessingCount++;
                pastGuess.add(tempGuess);
                if (tempGuess){
                    bfrGuess = 0;
                    altitudeStationary = usedAltitude;
                    altitudeStationaryInternal = bfrAltitude;
                }else{
                    bfrGuess = 1;
                    altitudeMoving  = usedAltitude;
                    altitudeMovingInternal = bfrAltitude;

                    altitudeMovOffset = altitudeMovOffset+altitudeStayOffset;
                    altitudeMovOffsetInternal = altitudeMovOffsetInternal+altitudeStayOffsetInternal;

                    altitudeStayOffset = 0;
                    altitudeStayOffsetInternal = 0;
                }
            }else{
                bfrGuess = 2;
                altitudeMoving = altitudeStationary = usedAltitude;
                altitudeMovingInternal = altitudeStationaryInternal = bfrAltitude;
            }
            if (guessingCount == maxGuessSamples){
                guessingCount = 0;
                estimateFuture();
                pastGuess.clear();
            }
            bfrGuessedBasePressure = bfrBasePressure;
            altitudeOffset = altitudeStayOffset+altitudeMovOffset;
            altitudeOffsetInternal = altitudeStayOffsetInternal+altitudeMovOffsetInternal;
            guessedAltitudeDelta = usedAltitude+altitudeOffset;
        }else{
            altitudeMovOffset = 0;
            altitudeStayOffset = 0;
            altitudeMovingInternal = 0;
            altitudeMovingInternal = 0;
            altitudeOffset = 0;
            guessingCount = 0;
            pastGuess.clear();
        }
        Log.d("estimatePressure1", "Delta maxmin:" + deltaMaxMin + " deltaMediaAttuale" + deltaMedianActual + " stima:" + stima + " Fermo?" + (stima>deltaMedianActual) + " offset" + altitudeOffset);
        Log.d("estimatePressure2", " sono stato fermo?" + bfrGuess+" altitudeStationary:" + altitudeStationary + " altitudeMoving:" + altitudeMoving + " altitudeOffset:" + altitudeOffset);
        Log.d("algoritmo",deltaMaxMin+","+deltaMedianActual+","+stima+","+usedAltitude+","+altitudeMoving+","
                +altitudeStationary+","+altitudeMovOffset+","+altitudeStayOffset+","+((stima>deltaMedianActual)? 1 : 0)+","+((tempGuess)? 1 : 0));
    }
    private void estimateFuture(){
        int stayCount = 0;
        int moveCount = 0;
        for (boolean guess:pastGuess) {
            if (guess){
                stayCount++;
            }else{
                moveCount++;
            }
        }
        if (stayCount >= moveCount){
            altitudeStayOffset = altitudeMoving-altitudeStationary;
            altitudeStayOffsetInternal = altitudeMovingInternal - altitudeStationaryInternal;
            Log.d("estimateShoot", "Provo a correggere!");
        }else{
            Log.d("estimateShoot", "Mi stavo muovendo!");
        }
    }
    private void findNewMinMax(){
        minPressure = maxPressure = pastPressures.get(0);
        for (Double pressure:pastPressures) {
            if (maxPressure < pressure){
                maxPressure = pressure;
            }
            if (minPressure > pressure){
                minPressure = pressure;
            }
        }
    }
    private boolean checkPastCondition(){
        int count = 0;
        for (boolean immoble:pastConditions) {
            if (!immoble){
                count++;
                if (count == movementError){
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate(){
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        try {
            maxPressureSamples = Integer.parseInt(SP.getString("maxPressureS", "25"));
            stimaDiv = Double.parseDouble(SP.getString("divMinMax", "1.65"));
            maxGuessSamples = Integer.parseInt(SP.getString("maxGuessS", "10"));
            movementError = Integer.parseInt(SP.getString("movError", "5"));
            hasRelay = Integer.parseInt(SP.getString("isRelay", "0"));
            useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
            delay = Integer.parseInt(SP.getString("beaconDelay","60"))*MS_TO_SECOND;
            guessingCount = 0;
        } catch(NumberFormatException e) {
            Log.e("NumberFormatException ", e.toString());
        }
        pastPressures = new ArrayList<>(maxPressureSamples);
        pastConditions = new ArrayList<>(maxPressureSamples);
        pastGuess = new ArrayList<>(maxGuessSamples);
        timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ITALY);
        IntentFilter filter = new IntentFilter(filterNewData);
        filter.addAction(filterSettings);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);
        mMessageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                mNearbyDevicesArrayList.add(pressureMessage.fromNearbyMessage(message).getMessageBody());
                mPressureMessageList.add(pressureMessage.fromNearbyMessage(message));
                Log.d("moireNearby", "Messaggi letti:" + mNearbyDevicesArrayList + "Pressione" + mPressureMessageList.get(mPressureMessageList.size()-1).getPressure());
            }

            @Override
            public void onLost(final Message message) {
                //mNearbyDevicesArrayAdapter.remove(pressureMessage.fromNearbyMessage(message).getMessageBody());
                Log.d("moireNearby", "Ho perso un messaggio");
            }
        };
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        timeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("moireNearby", "pubblico!");
                if (!firstPublish) {
                    Log.d("moireNearby", "Prima pulisco! " + mNearbyDevicesArrayList.size() + "|" + mPressureMessageList.size());
                    unpublish();
                    unsubscribe();
                    mNearbyDevicesArrayList.clear();
                    mPressureMessageList.clear();
                    Log.d("moireNearby", "Ho pulito! " + mNearbyDevicesArrayList.size() + "|" + mPressureMessageList.size());
                }
                publish();
                subscribe();
                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                try {
                    useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
                } catch (NumberFormatException e) {
                    Log.e("NumberFormatException ", e.toString());
                }
                if (useBeacon == 1) {
                    new getPressureFromBeacon().execute();
                }
                Log.d("beacon", "valore di beacon:" + useBeacon);
                firstPublish = false;
                timeHandler.postDelayed(this, delay);
            }
        }, delay);
        Log.d("moire", "sono attivo");
        Log.d("estimateSetting", "maxPressureSamples:" + maxPressureSamples + " stimaDiv:" + stimaDiv + " maxGuessSamples:" +
                maxGuessSamples + " movementError:" + movementError+" guessingCount:"+guessingCount+" hasRelay:"+hasRelay+" delay"+delay/MS_TO_SECOND);
    }

    public void estimatePresent(){

        int typeSelect;
        Double pressureSelect;
        Date datePub;
        Date datePressure;
        boolean haveToCorrect = false;
        /**********/
        double gpsMax=0.0;
        double gpsMin=5000.0;
        double gpsTot=0.0;
        int gpsCount = 0;
        double gpsGuess;

        double beaconMax=0.0;
        double beaconMin=5000.0;
        double beaconTot=0.0;
        int beaconCount = 0;
        double beaconGuess;

        boolean haveUnfixed = false;
        double  unfixedBest = 0.0;
        Date unfixedDate = lastPressureDate;
        /**********/

        for (pressureMessage device:mPressureMessageList) {
            //buffer = parseDevice(deviceValue);

            typeSelect = device.getType();
            pressureSelect = device.getPressure();
            datePressure = device.getDatePressure();
            //datestringSelect = buffer[3];
            switch(typeSelect){
                case 0:
                   //"LPS-STAY";
                    if (datePressure.after(unfixedDate)){
                        unfixedDate = datePressure;
                        unfixedBest = pressureSelect;
                        haveUnfixed = true;
                        haveToCorrect = true;
                    }
                    break;
                case 1:
                    //"LPS-MOVE";
                    if (datePressure.after(unfixedDate)){
                        unfixedDate = datePressure;
                        unfixedBest = pressureSelect;
                        haveUnfixed = true;
                        haveToCorrect = true;
                    }
                    break;
                case 2:
                    //"LPS-SOON";
                    if (datePressure.after(unfixedDate)){
                        unfixedDate = datePressure;
                        unfixedBest = pressureSelect;
                        haveUnfixed = true;
                        haveToCorrect = true;
                    }
                    break;
                case 3:
                    //"GPS";
                    if (datePressure.after(unfixedDate)) {
                        if (gpsMax <= pressureSelect) {
                            gpsMax = pressureSelect;
                        }
                        if (gpsMin <= pressureSelect) {
                            gpsMin = pressureSelect;
                        }
                        gpsTot += pressureSelect;
                        gpsCount++;
                        haveToCorrect = true;
                    }
                    break;
                case 4:
                    //"BEACON";
                    if (datePressure.after(unfixedDate)) {
                        if (beaconMax <= pressureSelect) {
                            beaconMax = pressureSelect;
                        }
                        if (beaconMin <= pressureSelect) {
                            beaconMin = pressureSelect;
                        }
                        beaconTot += pressureSelect;
                        beaconCount++;
                        haveToCorrect = true;
                    }
                    break;
                default:
                    //"UNKNOW";
                    break;
            }
        }
        if (haveToCorrect) {
            int correction;
            if (beaconTot > 0) {
                //aggiungere controllo se gli estremi sono eccessivi e rimuoverli?
                externalValue = beaconGuess = beaconTot / beaconCount;
                Log.d("estimatePresent", "beacon:" + beaconGuess);
                lastPressureDate = Calendar.getInstance().getTime();
            } else if (gpsTot > 0) {
                //aggiungere controllo se gli estremi sono eccessivi e rimuoverli?
                externalValue = gpsGuess = gpsTot / gpsCount;
                Log.d("estimatePresent", "gps:" + gpsGuess);
                lastPressureDate = Calendar.getInstance().getTime();
            } else if (haveUnfixed) {
                Log.d("estimatePresent", "unfixed:" + unfixedBest);
                externalValue = unfixedBest;
                lastPressureDate = unfixedDate;
            }
            correction = (int) SensorManager.getAltitude((float) externalValue, (float) bfrActualPressure);
            altitudeMovOffset = 0;
            altitudeStayOffset = 0;
            altitudeMoving = correction;
            altitudeStationary = correction;
            lastPressureDate = Calendar.getInstance().getTime();
            haveDevicesValues = true;
        }
    }

    private String[] parseDevice(String device){
        String[] buffer;
        buffer = device.split("\\|");
        return buffer;
    }

    @Override
    public void onDestroy(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        timeHandler.removeCallbacksAndMessages(null);
        if (mGoogleApiClient.isConnected()) {
            unpublish();
            unsubscribe();
            mGoogleApiClient.disconnect();
        }
        if (restartService){
            startService(new Intent(getApplicationContext(), fateService.class));
        }
        Log.d("moire", "sono spento");
    }
    /***********************************************************************************************/
    private void publish() {
        Log.i(TAG, "trying to publish");
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            PublishOptions options = new PublishOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new PublishCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer publishing");
                        }
                    }).build();

            Nearby.Messages.publish(mGoogleApiClient, mDeviceInfoMessage, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "published successfully");
                            } else {
                                Log.i(TAG, "could not publish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    private void unpublish() {
        Log.i(TAG, "trying to unpublish");
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unpublish(mGoogleApiClient, mDeviceInfoMessage)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unpublished successfully");
                            } else {
                                Log.i(TAG, "could not unpublish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void subscribe() {
        Log.i(TAG, "trying to subscribe");
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            //clearDeviceList();
            SubscribeOptions options = new SubscribeOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new SubscribeCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer subscribing");
                            if (bfrFix != 1) {
                                estimatePresent();
                            }
                        }
                    }).build();
            Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, options)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "subscribed successfully");
                            } else {
                                Log.i(TAG, "could not subscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void unsubscribe() {
        Log.i(TAG, "trying to unsubscribe");
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unsubscribed successfully");
                            } else {
                                Log.i(TAG, "could not unsubscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
   /***********************************************************************************************/

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // For simplicity, we don't handle connection failure thoroughly in this sample. Refer to
        // the following Google Play services doc for more details:
        // http://developer.android.com/google/auth/api-client.html
        Log.i(TAG, "connection to GoogleApiClient failed");
    }
    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: ");
    }

    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i(TAG, "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!mResolvingNearbyPermissionError) {
                Log.d("errore", "Non ho i permessi");
            }
        } else {
            if (status.getStatusCode() == ConnectionResult.NETWORK_ERROR) {
                Toast.makeText(getApplicationContext(),
                        "No connectivity, cannot proceed. Fix in 'Settings' and try again.",
                        Toast.LENGTH_LONG).show();
                //resetToDefaultState();
            } else {
                // To keep things simple, pop a toast for all other error messages.
                Toast.makeText(getApplicationContext(), "Unsuccessful: " +
                        status.getStatusMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
    }

    private class getPressureFromBeacon extends AsyncTask<Void, String, Void> {
        InputStream iStream = null;
        String result = "";

        String textUrl = "http://192.168.4.1/pressure";

        @Override
        protected Void doInBackground(Void... v) {
            try {

                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                textUrl = SP.getString("webserverAddress", "http://192.168.4.1/pressure");

                URL url = new URL(textUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000); // 10 secondi
                conn.setConnectTimeout(15000); //15 secondi
                conn.setDoInput(true); //permette input
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("BeaconURL", "L'url è:" + textUrl + " The response is: " + response);
                iStream = conn.getInputStream();
            }catch (UnsupportedEncodingException e1) {
                Log.e("UEE", e1.toString());
                e1.printStackTrace();
            } catch (IllegalStateException e2) {
                Log.e("IllegalStateException", e2.toString());
                e2.printStackTrace();
            } catch (IOException e3) {
                Log.e("IOException", e3.toString());
                e3.printStackTrace();
            }
            try {
                //converto il risultato in maniera leggibile
                BufferedReader bReader = new BufferedReader(new InputStreamReader(iStream, "utf-8"), 8);
                StringBuilder sBuilder = new StringBuilder();

                String line; //null
                while ((line = bReader.readLine()) != null) {
                    sBuilder.append(line).append("\n");
                }

                iStream.close();
                Log.d("BeaconURL", "Stream: " + sBuilder.toString());
                result = sBuilder.toString();

            } catch (Exception e) {
                Log.e("StrBuild", "Error converting " + e.toString());
            }
            return null;
        }

        //parso il JSON e lo restituisco come variabile
        protected void onPostExecute(Void v) {
                    Log.e("BeaconURL", "dovrei fare parsing");

            /*
            Message testMessaggio = pressureMessage.newNearbyMessage(
                    "B3aC0n", 4, 1010.10,timeFormatFile.format(Calendar.getInstance().getTime())
            );
            mPressureMessageList.add(pressureMessage.fromNearbyMessage(testMessaggio));
            mNearbyDevicesArrayList.add(pressureMessage.fromNearbyMessage(testMessaggio).getMessageBody());
            Log.d("beaconURL", "Messaggi letti:" + mNearbyDevicesArrayList + "Pressione" + mPressureMessageList.get(mPressureMessageList.size() - 1).getPressure());
            */
            try {
                JSONObject jObject = new JSONObject(result);
                Double jsonPressure = Double.parseDouble(jObject.optString("pressure"));
                Log.d("beaconURL", "Sensore:"+jsonPressure);
                Message beaconPressure = pressureMessage.newNearbyMessage(
                            "B3aC0n", 4,jsonPressure, timeFormatFile.format(Calendar.getInstance().getTime())
                );
                mPressureMessageList.add(pressureMessage.fromNearbyMessage(beaconPressure));
                mNearbyDevicesArrayList.add(pressureMessage.fromNearbyMessage(beaconPressure).getMessageBody());
                Log.d("beaconURL", "Messaggi letti:" + mNearbyDevicesArrayList + "Pressione" + mPressureMessageList.get(mPressureMessageList.size() - 1).getPressure());
            } catch (JSONException e) {
                Log.e("JSONException", "Error: " + e.toString());
            }
        }

    }

}
