package com.tapiri.lollum.espiel;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.widget.ArrayAdapter;
import android.widget.TextView;

import android.support.v4.content.LocalBroadcastManager;
import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;

import java.util.ArrayList;


public class mainActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private static final int COORDINATE_FORMAT = Location.FORMAT_SECONDS;
    double bfrLatitude;
    double bfrLongitude;
    int bfrAltitude;
    //boolean bfrFix;
    int bfrFix;
    int bfrElevation;
    float bfrAccuracy;

    int bfrGuess = 2; //0 fermo 1 in movimento 2 troppo presto

    double lastLatitude;
    double lastLongitude;
    float lastAccuracy;
    int coordinateFormat = COORDINATE_FORMAT;

    private GoogleMap mMap;
    private CircleOptions positionRadius = new CircleOptions();
    private CircleOptions myPosition = new CircleOptions();

    private String filterProData = "prodata";
    private String filterUpdate = "wantupdate";

    /*******************************************************/
    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder().setTtlSeconds(10).build(); // in secondi
    private GoogleApiClient mGoogleApiClient;
    private Message mDeviceInfoMessage;
    private boolean mResolvingNearbyPermissionError = false;
    /*******************************************************/


    //oltre a inizializzare l'activity attivo il servizio
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent gpsService = new Intent(this, positionService.class);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(filterProData));
        startService(gpsService);

        mDeviceInfoMessage = pressureMessage.newNearbyMessage(
                InstanceID.getInstance(getApplicationContext()).getId());
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        Intent oracleService = new Intent(this, fateService.class);
        startService(oracleService);

    }
    //prendo impostazioni dalle preferenze e inizializzo mappa, GUI e chiedo un update dal servizio
    @Override
    protected void onStart(){
        super.onStart();
        setUpMapIfNeeded();
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        try {
            coordinateFormat = Integer.parseInt(SP.getString("coordinateType", "0"));
        } catch(NumberFormatException e) {
            Log.e("NumberFormatException ", e.toString());
        }
        requestUpdate();
        updateGUI();
    }


    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, positionService.class));
        stopService(new Intent(this, fateService.class));
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    //ricevitore intent dal servizio
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(filterProData)){
                readFromProvider();
                updateGUI();
            }
        }
    };

    private void readFromProvider(){
        String URL_GPS = EspielProvider.GPS_URL;
        String URL_SENSOR = EspielProvider.USABLE_SENSOR_URL;
        Uri gps_data = Uri.parse(URL_GPS);
        Uri sensor_data = Uri.parse(URL_SENSOR);
        Cursor c_gps = getApplicationContext().getContentResolver().query(gps_data, null, null, null, null);
        Cursor c_sensor = getApplicationContext().getContentResolver().query(sensor_data, null, null, null, null);
        if (c_gps != null && c_gps.moveToFirst()){
            bfrElevation = c_gps.getInt(0);
            bfrLatitude = c_gps.getDouble(1);
            bfrLongitude = c_gps.getDouble(2);
            bfrFix = c_gps.getInt(3);
            bfrAccuracy = c_gps.getInt(4);
            c_gps.close();
        }
        if (c_sensor != null && c_sensor.moveToFirst()){
            bfrAltitude = c_sensor.getInt(2);
            bfrGuess = c_sensor.getInt(3);
            c_sensor.close();
        }
        Log.d("TestProviderActivity","bfrElevation:" +bfrElevation+" bfrLatitude:"+bfrLatitude+
                " bfrLongitude:"+ bfrLongitude+" bfrFix:"+ bfrFix+" bfrAccuracy:"+ bfrAccuracy+
                " bfrAltitude:"+ bfrAltitude+" bfrGuess:"+ bfrGuess);
    }

    //ausiliaria per aggiornare interfaccia utente
    private void updateGUI() {
        Log.d("UpdateGUI", "coordinate?" + bfrLatitude + ":" + bfrLongitude + ":" + bfrAccuracy);
        if (bfrLatitude != positionService.NO_COORDINATE && bfrLongitude != positionService.NO_COORDINATE && bfrAccuracy != positionService.NO_COORDINATE){
            lastLatitude = bfrLatitude;
            lastLongitude = bfrLongitude;
            lastAccuracy = bfrAccuracy;
        }

        if (lastLatitude != 0)
            updateText(R.id.textLatitude, Location.convert(lastLatitude,coordinateFormat));
        if (lastLongitude != 0)
            updateText(R.id.textLongitude, Location.convert(lastLongitude,coordinateFormat));
        if (bfrElevation != 0)
            updateText(R.id.textElevationValue, String.valueOf(bfrElevation)+getResources().getString(R.string.SeaLevel));
        switch(bfrFix){
            case positionService.LPS_PURE:
                updateText(R.id.textPosType,getResources().getString(R.string.PosTypeLPS));
                break;
            case positionService.GPS_FIX:
                updateText(R.id.textPosType,getResources().getString(R.string.PosTypeGPS));
                break;
            case positionService.LPS_FIX:
                updateText(R.id.textPosType,getResources().getString(R.string.PosTypeLPSfix));
                break;
        }
        updateText(R.id.textAltitudeValue, String.valueOf(bfrAltitude)+getResources().getString(R.string.Measure));
        if (bfrAltitude != 0){
            updateText(R.id.textFloorValue, String.valueOf(bfrAltitude/3)+getResources().getString(R.string.Grade));
        }else{
            updateText(R.id.textFloorValue, getResources().getString(R.string.GroundFloor));
        }


        if (lastLatitude != 0 && lastLongitude != 0){
            Log.d("GoogleMap", "Aggiorno mappa" + lastLatitude + ":" + lastLongitude);
            setUpMap(lastLatitude, lastLongitude,lastAccuracy);
        }

        switch (bfrGuess){
            case 0:
                updateText(R.id.movementState,getResources().getString(R.string.movementStay));
                break;
            case 1:
                updateText(R.id.movementState,getResources().getString(R.string.movementMove));
                break;
            case 2:
                updateText(R.id.movementState,getResources().getString(R.string.movementSoon));
                break;
            case 3:
                updateText(R.id.movementState,getResources().getString(R.string.movementGPS));
                break;
            default:
                updateText(R.id.movementState,getResources().getString(R.string.movementSoon));
                break;
        }
    }

    //ausiliaria per aggiornare campo text
    private void updateText(int id, String text){
        TextView textView = (TextView) findViewById(id);
        textView.setText(text);
    }

    //Autogenerato google, imposta la mappa se non e' gia impostata
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
    }

    //sposta la visione della mappa alla locazione effettuando uno zoom. Crea un pallino per la posizione ed un cerchio per accuratezza
    private void setUpMap(double lat, double lon, float accuracy) {
        Log.d("MAP", "Coordinate Ricevute"+lat+":"+lon);
        LatLng posLatLon = new LatLng(lat, lon);
        mMap.clear();
        positionRadius.center(posLatLon);
        positionRadius.radius(accuracy);
        positionRadius.strokeColor(Color.BLACK);
        positionRadius.strokeWidth(3);
        myPosition.center(posLatLon);
        myPosition.strokeColor(Color.RED);
        myPosition.radius(1);

        mMap.addCircle(positionRadius);
        mMap.addCircle(myPosition);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(posLatLon));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

    }

    //Autogenerato, popolo il menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem useBeaconMenu = menu.findItem(R.id.action_usebeacon);
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int useBeacon;
        try {
            useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
            if (useBeacon == 0){
                useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconNo));
            }else{
                useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconYes));
            }
        } catch(NumberFormatException e) {
            Log.e("NumberFormatException ", e.toString());
            useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconNo));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    //opzioni del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent iSelect;
        switch (item.getItemId()) {
            case R.id.action_settings:
                iSelect = new Intent(this, settingsActivity.class);
                startActivity(iSelect);
                break;
            case R.id.action_findme:
                //forza la mappa a trovarmi
                if (lastLatitude != 0 && lastLongitude != 0)
                    setUpMap(lastLatitude, lastLongitude,lastAccuracy);
                break;
            case R.id.action_buidlinginsert:
                iSelect = new Intent(this, buildingHandlerActivity.class);
                iSelect.putExtra("fragment", buildingHandlerActivity.FRAGMENT_INSERT);
                startActivity(iSelect);
                break;
            case R.id.action_buidlingview:
                iSelect = new Intent(this, buildingHandlerActivity.class);
                iSelect.putExtra("fragment", buildingHandlerActivity.FRAGMENT_VIEW);
                startActivity(iSelect);
                break;
            case R.id.action_usebeacon:
                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor SPeditor = SP.edit();
                int useBeacon;
                try {
                    useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
                    if (useBeacon == 0){
                        SPeditor.putString("useBeacon", "1");
                        item.setTitle(getResources().getString(R.string.useBeaconYes));
                    }else{
                        SPeditor.putString("useBeacon", "0");
                        item.setTitle(getResources().getString(R.string.useBeaconNo));
                    }
                } catch(NumberFormatException e) {
                    Log.e("NumberFormatException ", e.toString());
                    SPeditor.putString("useBeacon", "0");
                    item.setTitle(getResources().getString(R.string.useBeaconNo));
                }
                SPeditor.apply();
                break;
        default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //chiede un aggiornamento
    private void requestUpdate(){
        Log.d("wantupdate", "Mando aggiornamento");
        Intent intent = new Intent(filterUpdate);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    private void checkNerby() {
        Log.i("nearbyActivity", "trying to publish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            PublishOptions options = new PublishOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new PublishCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i("nearbyActivity", "no longer publishing");
                        }
                    }).build();

            Nearby.Messages.publish(mGoogleApiClient, mDeviceInfoMessage, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i("nearbyActivity", "published successfully");
                            } else {
                                Log.i("nearbyActivity", "could not publish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
            Nearby.Messages.unpublish(mGoogleApiClient, mDeviceInfoMessage)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i("nearbyActivity", "unpublished successfully");
                                mGoogleApiClient.disconnect();
                            } else {
                                Log.i("nearbyActivity", "could not unpublish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i("nearbyActivity", "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!mResolvingNearbyPermissionError) {
                try {
                    mResolvingNearbyPermissionError = true;
                    status.startResolutionForResult(this,
                            1001);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i("nearbyActivity", "GoogleApiClient connected");
        checkNerby();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // For simplicity, we don't handle connection failure thoroughly in this sample. Refer to
        // the following Google Play services doc for more details:
        // http://developer.android.com/google/auth/api-client.html
        Log.i("nearbyActivity", "connection to GoogleApiClient failed");
    }
    @Override
    public void onConnectionSuspended(int cause) {
        Log.i("nearbyActivity", "GoogleApiClient connection suspended: ");
    }
}
