package com.tapiri.lollum.espiel;

import android.app.Activity;
import android.app.ListFragment;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class buildingGraphFragment extends ListFragment {
    View view;
    File log;
    List<File> logs;
    floorsampleAdapter floorAdapter;
    ListView floorListView;
    List<String> floorSample;
    SimpleDateFormat hourFormat;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_building_graph, container, false);
        String fileName = getArguments().getString("log");
        if (fileName != null && !fileName.isEmpty()){
            log = new File(fileName);
        }
        Log.d("LOG_GRAPH",log.toString());

        floorSample = getFloorSamples(log);

        floorAdapter = new floorsampleAdapter(view.getContext(), floorSample);
        setListAdapter(floorAdapter);
        GraphView graph = (GraphView) view.findViewById(R.id.graph);


        LineGraphSeries<DataPoint> serieRealFloor = new LineGraphSeries<>(createPoints(floorSample,logService.LOG_RF));
        LineGraphSeries<DataPoint> serieCalcFloor = new LineGraphSeries<>(createPoints(floorSample,logService.LOG_CF));
        LineGraphSeries<DataPoint> serieRealAltitude = new LineGraphSeries<>(createPoints(floorSample,logService.LOG_RA));
        LineGraphSeries<DataPoint> serieCalcAltitude = new LineGraphSeries<>(createPoints(floorSample,logService.LOG_CA));


        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(5);
        graph.getGridLabelRenderer().setNumHorizontalLabels(6);
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setTextSize(17);

        graph.getGridLabelRenderer().setHighlightZeroLines(true);


        serieRealFloor.setThickness(8);
        serieRealFloor.setTitle("Piani reali");
        serieRealFloor.setColor(Color.BLUE);

        serieCalcFloor.setTitle("Piani calcolati");
        serieCalcFloor.setColor(Color.RED);

        serieRealAltitude.setThickness(8);
        serieRealAltitude.setTitle("Altitudine reale");
        serieRealAltitude.setColor(Color.CYAN);

        serieCalcAltitude.setTitle("Altitudine calcolata");
        serieCalcAltitude.setColor(Color.MAGENTA);

        //graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));

        graph.addSeries(serieRealFloor);
        graph.addSeries(serieCalcFloor);
        graph.addSeries(serieRealAltitude);
        graph.addSeries(serieCalcAltitude);

        //graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        //graph.getGridLabelRenderer().setNumHorizontalLabels(3);

        return view;
    }

    private DataPoint[] createPoints(List<String> floorSample,int value){
        DataPoint[] bufferArray = new DataPoint[floorSample.size()];
        int i= 0;
        for(String sample:floorSample){
            String[] parameter = sample.split(",");
            SimpleDateFormat timeFormatFile = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.ITALY);
            //ParsePosition pos = new ParsePosition(0);
            //Date dateTime = timeFormatFile.parse(parameter[13], pos);
            bufferArray[i] = new DataPoint(i,Double.valueOf(parameter[value]));
            i++;
        }
        return bufferArray;
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    private List<String> getFloorSamples(File log){
        List<String> bfrSample = new ArrayList<>();



        FileInputStream is;
        BufferedReader reader;

        if (log.exists()) {
            try{
                is = new FileInputStream(log);
                reader = new BufferedReader(new InputStreamReader(is));
                String line = reader.readLine();
                while(line != null){
                    //Log.d("LOG_LINE", line);
                    bfrSample.add(line);
                    line = reader.readLine();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }


        return bfrSample;
    }

}
