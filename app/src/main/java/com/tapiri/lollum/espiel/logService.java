package com.tapiri.lollum.espiel;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.nearby.messages.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class logService extends Service {
    private static final float FLOOR_DELTA = 3;

    static final int FLOOR_START = 0;
    static final int FLOOR_STAY = 1;
    static final int FLOOR_MOVE = 2;
    static final int FLOOR_STOP = 3;
    static final String FLOOR_START_STRING = "START";
    static final String FLOOR_STAY_STRING = "STAY";
    static final String FLOOR_MOVE_STRING = "MOVE";
    static final String FLOOR_STOP_STRING = "STOP";
    static final String FLOOR_UNKNOWN_STRING = "UNKNOWN";

    static final int SINGLEPRESSURE = 0;
    static final int DOUBLEPRESSURE = 1;
    static final int BEACON = 2;
    static final int ALGO = 3;
    static final int BEACONALGO = 4;
    static final int BEACONFIXED = 5;
    static final int BEACONALGOFIXED = 6;

    static final int LOG_TYPE= 0;//0
    static final int LOG_LAT= LOG_TYPE+1;//1
    static final int LOG_LON= LOG_LAT+1;//2
    static final int LOG_EL= LOG_LON+1;//3
    static final int LOG_SP= LOG_EL+1;//4
    static final int LOG_ST = LOG_SP+1;//5
    static final int LOG_FP= LOG_ST+1;//6
    static final int LOG_FT= LOG_FP+1;//7
    static final int LOG_RF= LOG_FT+1;//8
    static final int LOG_RA= LOG_RF+1;//9
    static final int LOG_CF= LOG_RA+1;//10
    static final int LOG_CA= LOG_CF+1;//11
    static final int LOG_IP= LOG_CA+1;//12
    static final int LOG_PREC= LOG_IP+1;//13
    static final int LOG_TIME= LOG_PREC+1;//14
    static final int LOG_OFFSET= LOG_TIME+1;//15
    static final int LOG_EXTVAL= LOG_OFFSET+1;//16
    static final int LOG_OFFSETLOC= LOG_EXTVAL+1;//17
    static final int LOG_PUREALT= LOG_OFFSETLOC+1;//18
    static final int LOG_GUESS= LOG_PUREALT+1;//19
    static final int LOG_GPSFIX= LOG_GUESS+1;//19
    static final int LOG_BEACONOFFSET= LOG_GPSFIX+1;//19



    private static final String FNS = "_"; // file name separator
    private static final String COMMA = ","; // file name separator
    private static final int MS_TO_SECOND = 1000;

    private static final boolean APPEND_LINE= true;

    // NOME FILE
    private String name;
    private String address;
    private double lastLatitude;
    private double lastLongitude;

    //INIZIO E USCITA
    private double initialPressure;
    private Date initialTime;
    private double finalPressure = -1;
    private Date finalTime = null;

    //CAMPIONAMENTO
    private int sampleType = 1;
    private int lastElevation;
    private String realFloor="0";
    private String realAltitude="0";
    private String nextRealFloor="0";
    private String nextRealAltitude="0";
    private String calculatedFloor="0";
    private int calculatedAltitude=0;
    private double instantPressure;
    private double localBasePressure = 0.0;
    private int localCorrectedAlt = 0;
    private Date instantTime;
    private double precision=-1;
    private int offset = 0;
    private int offsetLocal = 0;
    private int purealtitude = 0;
    private int guessvalue = 2;
    private int gpsfix = 0;
    private double externalValue = 0.0;
    private double basePressure = 0.0;

    int pressureBeaconDelta = 0;

    Handler timeHandler = new Handler();
    int delay = MS_TO_SECOND; //milliseconds
    //Date time;
    SimpleDateFormat timeFormat;
    private int timerDelay = MS_TO_SECOND;
    private int valueCountdown = 20;
    private int timerCountdown = valueCountdown*MS_TO_SECOND;

    File singlepressureLog;
    File doublepressureLog;
    File beaconLog;
    File algoLog;
    File beaconalgoLog;
    File bafixedLog;
    File beaconfixedLog;

    File dirLog;
    File dirSingleLog;

    boolean firstExternalBeacon = true;
    boolean firstExternalBA = true;
    int offsetbeacon = 0;

    private String filterProData = "prodata";
    private String filterFloor = "floor";
    private String filterUpdate = "wantupdate";
    private String filterCountdown = "countdown";
    public logService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("LOGSERVICE", "Servizio attivo!");
        IntentFilter filter = new IntentFilter(filterProData);
        filter.addAction(filterFloor);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(mMessageReceiver, filter);
        requestUpdate();
        readFromProvider();
        name = intent.getStringExtra("name");
        address = intent.getStringExtra("address");
        lastLatitude = intent.getDoubleExtra("latitude", positionService.NO_COORDINATE);
        lastLongitude = intent.getDoubleExtra("longitude", positionService.NO_COORDINATE);
        instantPressure = intent.getDoubleExtra("pressure", 0);
        precision = intent.getDoubleExtra("pressureaccuracy", 0);
        initialPressure = localBasePressure = basePressure = intent.getDoubleExtra("basepressure",-1.0);
        timeFormat = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.ITALY);
        initialTime = instantTime = Calendar.getInstance().getTime();
        Log.d("logserviceStart", "name"+name+"address"+address+"lastLatitude"+lastLatitude+
                "lastLongitude"+lastLongitude+"instantPressure"+instantPressure);
        singlepressureLog=startLog("SINGLE",SINGLEPRESSURE);
        doublepressureLog=startLog("DOUBLE",DOUBLEPRESSURE);
        beaconLog=startLog("BEACON",BEACON);
        algoLog=startLog("ALGO",ALGO);
        beaconalgoLog =startLog("BEACONALGO",BEACONALGO);
        beaconfixedLog =startLog("BEACONFIXED",BEACONFIXED);
        bafixedLog =startLog("BAFIXED",BEACONALGOFIXED);
        new getPressureDeltaBeacon().execute();
        timeHandler.postDelayed(new Runnable() {
            public void run() {
                instantTime = Calendar.getInstance().getTime();
                writeLogLine(singlepressureLog,sampleType,SINGLEPRESSURE);
                writeLogLine(doublepressureLog,sampleType,DOUBLEPRESSURE);
                writeLogLine(beaconLog,sampleType,BEACON);
                writeLogLine(algoLog,sampleType,ALGO);
                writeLogLine(beaconalgoLog,sampleType,BEACONALGO);
                writeLogLine(beaconfixedLog,sampleType,BEACONFIXED);
                writeLogLine(bafixedLog,sampleType,BEACONALGOFIXED);
                timeHandler.postDelayed(this, delay);
            }
        }, delay);
        return START_STICKY;
    }

    @Override
    public void onCreate(){

    }

    @Override
    public void onDestroy(){
        stopLog(singlepressureLog,SINGLEPRESSURE);
        stopLog(doublepressureLog,DOUBLEPRESSURE);
        stopLog(beaconLog,BEACON);
        stopLog(algoLog,ALGO);
        stopLog(beaconalgoLog,BEACONALGO);
        stopLog(beaconfixedLog,BEACONFIXED);
        stopLog(bafixedLog,BEACONALGOFIXED);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void requestUpdate(){
        Log.d("wantupdate", "Mando aggiornamento");
        Intent intent = new Intent(filterUpdate);
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
    }

    //ricevitore per localbroadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("logservice","ricevuto qualcosa nel servizio di log");
            if (intent.getAction().equals(filterProData)) {
                readFromProvider();
            }else{
                if (intent.getAction().equals(filterFloor)){
                    nextRealFloor = intent.getStringExtra("realfloor");
                    nextRealAltitude = intent.getStringExtra("realaltitude");
                    Log.d("logging_floor", "RICEVO piani"+nextRealFloor+":"+nextRealAltitude);
                    startTransition();

                }
            }
        }
    };

    private void readFromProvider(){
        String URL_GPS = EspielProvider.GPS_URL;
        String URL_SENSOR = EspielProvider.USABLE_SENSOR_URL;
        String URL_PLUS = EspielProvider.PLUS_URL;

        Uri gps_data = Uri.parse(URL_GPS);
        Uri sensor_data = Uri.parse(URL_SENSOR);
        Uri plus_data = Uri.parse(URL_PLUS);

        Cursor c_gps = getApplicationContext().getContentResolver().query(gps_data, null, null, null, null);
        Cursor c_sensor = getApplicationContext().getContentResolver().query(sensor_data,null,null,null,null);
        Cursor c_plus = getApplicationContext().getContentResolver().query(plus_data, null, null, null, null);
        if (c_gps != null && c_gps.moveToFirst()){
            lastElevation = c_gps.getInt(0);
            lastLatitude = c_gps.getDouble(1);
            lastLongitude = c_gps.getDouble(2);
            gpsfix = c_gps.getInt(3);
            c_gps.close();
        }
        if (c_sensor != null && c_sensor.moveToFirst()){
            calculatedAltitude = c_sensor.getInt(2);
            calculatedFloor = calculateFloor(calculatedAltitude);
            instantPressure = c_sensor.getDouble(1);
            initialPressure = c_sensor.getDouble(0);
            guessvalue = c_sensor.getInt(3);
            precision = c_sensor.getDouble(4);
            c_sensor.close();
        }
        if (c_plus != null && c_plus.moveToFirst()){
            offset = c_plus.getInt(4);
            externalValue = c_plus.getDouble(1);
            localBasePressure = c_plus.getDouble(0);
            localCorrectedAlt = c_plus.getInt(3);
            offsetLocal = c_plus.getInt(5);
            purealtitude = c_plus.getInt(2);
            c_plus.close();
        }
        Log.d("TestProviderLog","lastElevation:" + lastElevation+" lastLatitude:"+lastLatitude+
                " calculatedAltitude:"+calculatedAltitude+ " calculatedFloor:"+calculatedFloor+
                " instantPressure:"+instantPressure+" initialPressure:"+initialPressure+
                " precision:"+precision+" offset:"+offset+" externalValue:"+externalValue+
                " localBasePressure:"+localBasePressure+" localCorrectedAlt:"+localCorrectedAlt);
    }

    private String calculateFloor(float altitude){
        return String.valueOf((int)(altitude/FLOOR_DELTA));
    }

    private File startLog(String info, int type){
        File newLog;
        if (canCreateFile()){
            Date timeFile = Calendar.getInstance().getTime();
            SimpleDateFormat timeFormatFile = new SimpleDateFormat("yyyyMMdd_HHmm",Locale.ITALY);
            File root = android.os.Environment.getExternalStorageDirectory();
            dirLog = new File (root.getAbsolutePath() + "/Espiel_Logs/"+timeFormatFile.format(timeFile.getTime())+FNS+name.replaceAll("\\s+","-")+FNS+address.replaceAll("\\s+","-"));
            if (dirLog.mkdirs()){
                Toast.makeText(getBaseContext(), "Creata directory" + dirLog.toString(), Toast.LENGTH_SHORT).show();
            }
            newLog = new File(dirLog, timeFormatFile.format(timeFile.getTime())+FNS+name.replaceAll("\\s+","-")+FNS+address.replaceAll("\\s+","-")+FNS+info+".csv");
            Toast.makeText(getBaseContext(),"path: "+newLog.getAbsolutePath(),Toast.LENGTH_LONG).show();
        } else {
            newLog = null;
        }
        finalPressure = -1;
        finalTime = null;
        writeLogLine(newLog,FLOOR_START,type);
        return newLog;
    }

    private void stopLog(File logFile, int type){
        timeHandler.removeCallbacksAndMessages(null);
        finalPressure = instantPressure;
        finalTime = Calendar.getInstance().getTime();
        writeLogLine(logFile,FLOOR_STOP,type);
    }

    private void writeLogLine(File log, int transition, int type){
        String sampletype;
        String finalPressureValue = "null";
        String finalTimeValue = "null";
        String finalLastLatitude = "null";
        String finalLastLongitude = "null";
        int selectedAltitude = 0;
        double selectedBasePressure = 0;
        String selectedPlane;
        switch (type){
            case SINGLEPRESSURE:
                selectedBasePressure = SensorManager.PRESSURE_STANDARD_ATMOSPHERE;
                selectedAltitude = (int)SensorManager.getAltitude((float)selectedBasePressure,(float)instantPressure);
                break;
            case DOUBLEPRESSURE:
                selectedBasePressure = localBasePressure;
                selectedAltitude = (int)SensorManager.getAltitude((float)selectedBasePressure,(float)instantPressure);
                break;
            case BEACON:
                if (externalValue == 0.0){
                    selectedBasePressure = localBasePressure;
                }else{
                    selectedBasePressure = externalValue;
                }
                selectedAltitude = (int)SensorManager.getAltitude((float)selectedBasePressure,(float)instantPressure);
                break;
            case ALGO:
                selectedAltitude = localCorrectedAlt;
                selectedBasePressure = localBasePressure;
                break;
            case BEACONALGO:
                if (externalValue == 0.0){
                    selectedBasePressure = localBasePressure;
                }else{
                    selectedBasePressure = externalValue;
                }
                selectedAltitude = calculatedAltitude;
                break;
            case BEACONFIXED:
                if (externalValue == 0.0){
                    selectedBasePressure = localBasePressure;
                }else{
                    selectedBasePressure = externalValue;
                    if (firstExternalBeacon){
                        offsetbeacon = pressureBeaconDelta; //(int)SensorManager.getAltitude((float)localBasePressure,(float)externalValue);
                        firstExternalBeacon=false;
                    }
                }
                selectedAltitude = ((int)SensorManager.getAltitude((float)selectedBasePressure,(float)instantPressure))+offsetbeacon;
                break;
            case BEACONALGOFIXED:
                if (externalValue == 0.0){
                    selectedBasePressure = localBasePressure;
                }else{
                    selectedBasePressure = externalValue;
                    if (firstExternalBA){
                        offsetbeacon = pressureBeaconDelta; //(int)SensorManager.getAltitude((float)localBasePressure,(float)externalValue);
                        firstExternalBA=false;
                    }
                }
                selectedAltitude = calculatedAltitude+offsetbeacon;
                break;
            default:
                break;
        }
        selectedPlane = calculateFloor(selectedAltitude);
        if (lastLatitude != positionService.NO_COORDINATE && lastLongitude != positionService.NO_COORDINATE){
            finalLastLatitude = String.valueOf(lastLatitude);
            finalLastLongitude = String.valueOf(lastLongitude);
        }
        if (finalPressure != -1 && finalTime != null){
            finalPressureValue = String.valueOf(finalPressure);
            finalTimeValue = timeFormat.format(finalTime.getTime());
        }
        try{
            FileOutputStream f = new FileOutputStream(log, APPEND_LINE);
            PrintWriter pw = new PrintWriter(f);
            StringBuilder newEntry = new StringBuilder(300);
            newEntry.append(transition).append(COMMA);  //0 TIPO
            newEntry.append(finalLastLatitude).append(COMMA); //1 LATITUDINE
            newEntry.append(finalLastLongitude).append(COMMA); //2 LONGITUDINE
            newEntry.append(lastElevation).append(COMMA); //3 ELEVATION
            newEntry.append(initialPressure).append(COMMA); //4 PRESSIONE INIZIALE
            newEntry.append(timeFormat.format(initialTime.getTime())).append(COMMA); //5 TEMPO INIZIALE
            newEntry.append(finalPressureValue).append(COMMA); //6 PRESSIONE FINALE
            newEntry.append(finalTimeValue).append(COMMA); //7 TEMPO FINALE
            newEntry.append(realFloor).append(COMMA);//8 PIANO REALE
            newEntry.append(realAltitude).append(COMMA);//9 ALTITUDINE REALE
            //newEntry.append(calculatedFloor).append(COMMA);//10 PIANO CALCOLATO
            newEntry.append(selectedPlane).append(COMMA);
            //newEntry.append(calculatedAltitude).append(COMMA);//11 ALTITUDINE CALCOLATA
            newEntry.append(selectedAltitude).append(COMMA);
            newEntry.append(instantPressure).append(COMMA);//12 PRESSIONE ISTANTANEA
            newEntry.append(precision).append(COMMA);//13 PRECISIONE SENSORE
            newEntry.append(timeFormat.format(instantTime.getTime())).append(COMMA);//14 TEMPO CAMPIONAMENTO
            newEntry.append(offset).append(COMMA);//15 OFFSET ALTITUDE GENERALE
            newEntry.append(externalValue).append(COMMA);//16 VALORE ESTERNO
            newEntry.append(selectedBasePressure).append(COMMA);//17 PRESSIONE BASE USATA
            newEntry.append(offsetLocal).append(COMMA);//18 OFFSET ALGORITMO LOCALE
            newEntry.append(purealtitude).append(COMMA);//19 altitudine pura
            newEntry.append(guessvalue).append(COMMA);//20 valore guess
            newEntry.append(gpsfix).append(COMMA);//21 fix
            newEntry.append(pressureBeaconDelta);//22 beacon offset

            newEntry.trimToSize();
            pw.println(newEntry.toString());
            pw.flush();
            pw.close();
            Log.d("LogserviceWriteOk", "Dovrei aver scritto!");
        }catch (IOException e) {
            e.printStackTrace();
            Log.d("LogserviceWriteError","Andato qualcosa storto"+e.toString());
        }
    }

    private boolean canCreateFile(){
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);

    }

    private void startTransition(){
        sampleType = FLOOR_MOVE;
        new CountDownTimer(timerCountdown+1000, timerDelay) {

            int countdown = valueCountdown;

            public void onTick(long millisUntilFinished) {
                if (countdown == (valueCountdown/2)){
                    realFloor = nextRealFloor;
                    realAltitude = nextRealAltitude;
                }
                sendCoundownState(countdown);
                countdown--;
            }

            public void onFinish() {
                sampleType = FLOOR_STAY;
                sendCoundownState(countdown);
            }
        }.start();
    }

    private void sendCoundownState(int countdownvalue) {
        Log.d("sendCoundownState", "Mando aggiornamento");
        Intent intent = new Intent(filterCountdown);
        intent.putExtra("value", countdownvalue);
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
    }

    private class getPressureDeltaBeacon extends AsyncTask<Void, String, Void> {
        InputStream iStream = null;
        String result = "";

        String textUrl = "http://192.168.4.1/pressure";

        @Override
        protected Void doInBackground(Void... v) {
            try {

                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                textUrl = SP.getString("webserverAddress", "http://192.168.4.1/pressure");

                URL url = new URL(textUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(30000); // 30 secondi
                conn.setConnectTimeout(60000); //15 secondi
                conn.setDoInput(true); //permette input
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("LogBeaconUrl", "L'url è:" + textUrl + " The response is: " + response);
                iStream = conn.getInputStream();
            }catch (UnsupportedEncodingException e1) {
                Log.e("UEE", e1.toString());
                e1.printStackTrace();
            } catch (IllegalStateException e2) {
                Log.e("IllegalStateException", e2.toString());
                e2.printStackTrace();
            } catch (IOException e3) {
                Log.e("IOException", e3.toString());
                e3.printStackTrace();
            }
            try {
                //converto il risultato in maniera leggibile
                BufferedReader bReader = new BufferedReader(new InputStreamReader(iStream, "utf-8"), 8);
                StringBuilder sBuilder = new StringBuilder();

                String line; //null
                while ((line = bReader.readLine()) != null) {
                    sBuilder.append(line).append("\n");
                }

                iStream.close();
                Log.d("BeaconURL", "Stream: " + sBuilder.toString());
                result = sBuilder.toString();

            } catch (Exception e) {
                Log.e("StrBuild", "Error converting " + e.toString());
            }
            return null;
        }

        //parso il JSON e lo restituisco come variabile
        protected void onPostExecute(Void v) {
            Log.e("BeaconURL", "dovrei fare parsing");
            try {
                JSONObject jObject = new JSONObject(result);
                double jsonPressure = Double.parseDouble(jObject.optString("pressure"));
                pressureBeaconDelta = (int)SensorManager.getAltitude((float)localBasePressure,(float)jsonPressure);
                Log.d("beaconURL", "Sensore:"+jsonPressure+" pressureBeaconDelta:"+pressureBeaconDelta);
            } catch (JSONException e) {
                Log.e("JSONException", "Error: " + e.toString());
            }
        }

    }
}
