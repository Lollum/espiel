package com.tapiri.lollum.espiel;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ShareActionProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class buildingHandlerActivity extends FragmentActivity {
    static final int FRAGMENT_INSERT = 0;
    static final int FRAGMENT_VIEW = 1;
    static final int FRAGMENT_GRAPH = 2;
    Menu menu;
    private FragmentManager fm;
    private FragmentTransaction fragmentTransaction;
    ShareActionProvider mShareActionProvider;

    //in base a cosa e' stato chiamato uso la fragment di inserimento O quella di vista
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Fragment frame;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_handler);
        int invokedFragment = getIntent().getIntExtra("fragment", 0);
        switch(invokedFragment){
            case FRAGMENT_INSERT:
                frame = new buildingInsertFragment();
                fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, frame);
                fragmentTransaction.commit();
                break;
            case FRAGMENT_VIEW:
                frame = new buildingViewFragment();
                fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, frame);
                fragmentTransaction.commit();
                break;
            case FRAGMENT_GRAPH:
                Bundle bundle=new Bundle();
                bundle.putString("log", getIntent().getStringExtra("log"));
                frame = new buildingGraphFragment();
                frame.setArguments(bundle);
                fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, frame);
                fragmentTransaction.commit();
                break;
            default:
                break;
        }
    }



    //popolo il menu
    @Override
    public boolean onCreateOptionsMenu(Menu newmenu) {
        menu = newmenu;
        getMenuInflater().inflate(R.menu.menu_building_handler, menu);
        //showShareAction(false);
        return true;
    }

    public void showShareAction(boolean show){
        if(menu == null)
            return;
        menu.findItem(R.id.menu_item_share).setVisible(show).setEnabled(show);
    }

    public List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
                if(file.getName().endsWith(".csv")){
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem useBeaconMenu = menu.findItem(R.id.action_usebeacon);
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int useBeacon;
        try {
            useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
            if (useBeacon == 0){
                useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconNo));
            }else{
                useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconYes));
            }
        } catch(NumberFormatException e) {
            Log.e("NumberFormatException ", e.toString());
            useBeaconMenu.setTitle(getResources().getString(R.string.useBeaconNo));
        }
        return super.onPrepareOptionsMenu(menu);
    }
    //selezion del menu, chiamo intent se devo andare alle impostazioni, altrimenti scambio il fragment
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent iSettings = new Intent(this, settingsActivity.class);
                startActivity(iSettings);
                break;
            case R.id.action_buidlinginsert:
                Fragment fBuildingInsert;
                fBuildingInsert = new buildingInsertFragment();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fBuildingInsert);
                //showShareAction(false);
                fragmentTransaction.commit();
                break;
            case R.id.action_buidlingview:
                Fragment fBuildingView;
                fBuildingView = new buildingViewFragment();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fBuildingView);
                //showShareAction(true);
                fragmentTransaction.commit();
                break;
            case R.id.action_usebeacon:
                SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor SPeditor = SP.edit();
                int useBeacon;
                try {
                    useBeacon = Integer.parseInt(SP.getString("useBeacon", "0"));
                    if (useBeacon == 0){
                        SPeditor.putString("useBeacon", "1");
                        item.setTitle(getResources().getString(R.string.useBeaconYes));
                    }else{
                        SPeditor.putString("useBeacon", "0");
                        item.setTitle(getResources().getString(R.string.useBeaconNo));
                    }
                } catch(NumberFormatException e) {
                    Log.e("NumberFormatException ", e.toString());
                    SPeditor.putString("useBeacon", "0");
                    item.setTitle(getResources().getString(R.string.useBeaconNo));
                }
                SPeditor.apply();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
