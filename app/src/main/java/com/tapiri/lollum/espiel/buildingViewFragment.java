package com.tapiri.lollum.espiel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class buildingViewFragment extends ListFragment {
    View view;

    ListView floorListView;

    List<File> logs;
    List<String> floorSample;

    private ProgressBar spinner;

    Dialog dialogFloors;
    AlertDialog.Builder builder;
    int lastBuildingPosition;
    File dirLog;

    floorsampleAdapter floorAdapter;
    ShareActionProvider mShareActionProvider;

    //quando il fragment diventa visibile collego il db e popolo la lista tramite adapter standard
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.building_view, container, false);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) shareItem.getActionProvider();
        mShareActionProvider.setShareIntent(createShareIntent());
    }

    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.setType("file/*");
        File dirLog = new File (android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/Espiel_Logs");
        List<File> logs = getListFiles(dirLog);
        ArrayList<Uri> uris = new ArrayList<>();
        for (File log: logs){
            uris.add(Uri.fromFile(log));
        }
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);
        return shareIntent;
    }

    @Override
    public void onStart(){
        super.onStart();
        dirLog = new File (android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/Espiel_Logs");
        logs = getListFiles(dirLog);

        //ArrayAdapter<File> buildingAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, files);
        buildingAdapter buildings = new buildingAdapter(view.getContext(), logs);
        //getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        setListAdapter(buildings);
    }

    public List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
                if(file.getName().endsWith(".csv")){
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

    //se viene premuto un elemento della lista restituisco i piani del suddetto palazzo
    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);
        lastBuildingPosition = position;
        Intent iSelect = new Intent(getActivity(), buildingHandlerActivity.class);
        iSelect.putExtra("fragment", buildingHandlerActivity.FRAGMENT_GRAPH);
        iSelect.putExtra("log", logs.get(position).toString());
        startActivity(iSelect);


    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }


    //dopo che l'activity viene creata se un palazzo viene premuto a lungo chiedo cancellazione, piani compresi
    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        spinner = (ProgressBar)getActivity().findViewById(R.id.spinner);
        spinner.setVisibility(View.GONE);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           final int arg2, long arg3) {
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Cancellazione palazzo")
                        .setMessage("Sei sicuro di voler cancellare il palazzo "+logs.get(arg2).getName()+"?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (logs.get(arg2).delete()){
                                    logs = getListFiles(dirLog);
                                    buildingAdapter buildings = new buildingAdapter(view.getContext(), logs);
                                    setListAdapter(buildings);
                                }else{
                                    Toast.makeText(view.getContext(),"Non sono riuscito a cancellare il file",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            }
        });

    }

    private class loadSamples extends AsyncTask<Integer, Void, List<String>> {


        @Override
        protected List<String> doInBackground(Integer... params) {
            floorSample = getFloorSamples(logs.get(params[0]));
            floorAdapter = new floorsampleAdapter(getActivity().getBaseContext(), floorSample);
            return floorSample;
        }

        @Override
        protected void onPostExecute(List<String> result) {
            floorListView.setAdapter(floorAdapter);
            builder.setView(floorListView);
            dialogFloors = builder.create();
            dialogFloors.show();
            spinner.setVisibility(View.GONE);
        }

        @Override
        protected void onPreExecute() {
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


    private List<String> getFloorSamples(File log){
        List<String> bfrSample = new ArrayList<>();



        FileInputStream is;
        BufferedReader reader;

        if (log.exists()) {
            try{
                is = new FileInputStream(log);
                reader = new BufferedReader(new InputStreamReader(is));
                String line = reader.readLine();
                while(line != null){
                    Log.d("LOG_LINE", line);
                    bfrSample.add(line);
                    line = reader.readLine();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }


        return bfrSample;
    }

}
