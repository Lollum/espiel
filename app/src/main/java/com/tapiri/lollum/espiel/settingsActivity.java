package com.tapiri.lollum.espiel;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class settingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            //aggiungo tutte le opzioni dall'xml settings
            addPreferencesFromResource(R.xml.settings);
            //aggiungo opzioni non disponibili nell'xml settings

            //aggiungo pulsante per resettare il db, prima di compiere l'operazione viene chiesta conferma
            Preference resetDatabase = findPreference("resetDatabase");
            resetDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {


                    new AlertDialog.Builder(getActivity())
                            .setTitle("Cancellazione piano")
                            .setMessage("Sei sicuro di voler cancellare i log?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    int i=0;
                                    List<File> logsDelete = getListFiles(new File (android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/Espiel_Logs"));
                                    for (File log: logsDelete){
                                        if (log.delete()){
                                            i++;
                                        }
                                    }
                                    Toast.makeText(getActivity(),String.format("Cancellati %1$d log",i),Toast.LENGTH_LONG).show();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;
                }
            });

        }


        public List<File> getListFiles(File parentDir) {
            ArrayList<File> inFiles = new ArrayList<>();
            File[] files = parentDir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    inFiles.addAll(getListFiles(file));
                } else {
                    if(file.getName().endsWith(".csv")){
                        inFiles.add(file);
                    }
                }
            }
            return inFiles;
        }

        @Override
        public void onStop(){
            super.onStop();
            updateSettingService();
        }

        //avverto il servizio di andare ad aggiornare parametri
        private void updateSettingService(){
            Log.d("updateSettings", "Mando aggiornamento");
            Intent intent = new Intent("settings");
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }
    }

}