package com.tapiri.lollum.espiel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class floorsampleAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<String> values;
    private Date startDate;
    private Date istantDate;
    SimpleDateFormat timeFormatFile;

    public floorsampleAdapter(Context context, List<String> values) {
        super(context, R.layout.building_adapter, values);
        this.context = context;
        this.values = values;
        timeFormatFile = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.ITALY);
        ParsePosition pos = new ParsePosition(0);
        startDate = timeFormatFile.parse(values.get(0).split(",")[logService.LOG_TIME], pos);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.floorsample_adapter, parent, false);
        }
        ImageView imageType = (ImageView) convertView.findViewById(R.id.transitiontype);
        TextView textRealFloor = (TextView) convertView.findViewById(R.id.realfloorvalue);
        TextView textRealAltitude = (TextView) convertView.findViewById(R.id.realaltitudevalue);
        TextView textCalcFloor = (TextView) convertView.findViewById(R.id.calcfloorvalue);
        TextView textCalcAltitude = (TextView) convertView.findViewById(R.id.calcaltitudevalue);
        TextView textPressure = (TextView) convertView.findViewById(R.id.pressure);
        TextView textTime = (TextView) convertView.findViewById(R.id.sampleTime);
        TextView textSeconds = (TextView) convertView.findViewById(R.id.seconds);
        String[] currentline = values.get(position).split(",");
        String time = currentline[logService.LOG_TIME].split("-")[1];

        switch (Integer.valueOf(currentline[logService.LOG_TYPE])){
            case logService.FLOOR_START:
                imageType.setImageResource(android.R.drawable.ic_media_next);
                break;
            case logService.FLOOR_STAY:
                imageType.setImageResource(android.R.drawable.ic_media_pause);
                break;
            case logService.FLOOR_MOVE:
                imageType.setImageResource(android.R.drawable.ic_media_play);
                break;
            case logService.FLOOR_STOP:
                imageType.setImageResource(android.R.drawable.ic_media_previous);
                break;
            default:
                imageType.setImageResource(android.R.drawable.ic_notification_clear_all);
                break;
        }

        ParsePosition pos = new ParsePosition(0);
        istantDate = timeFormatFile.parse(currentline[logService.LOG_TIME], pos);
        long diffInMs = istantDate.getTime() - startDate.getTime();

        textRealFloor.setText(currentline[logService.LOG_RF]);
        textRealAltitude.setText(currentline[logService.LOG_RA]);
        textCalcFloor.setText(currentline[logService.LOG_CF]);
        textCalcAltitude.setText(currentline[logService.LOG_CA]);
        textPressure.setText(currentline[logService.LOG_IP]);
        textTime.setText(time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4,6));
        textSeconds.setText(String.valueOf(TimeUnit.MILLISECONDS.toSeconds(diffInMs)));
        return convertView;
    }
} 

