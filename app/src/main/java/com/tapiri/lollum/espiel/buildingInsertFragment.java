package com.tapiri.lollum.espiel;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class buildingInsertFragment extends Fragment {
    Button btnBuildingEntrance;
    Button btnBuildingFloor;
    EditText edtNameValue;
    EditText edtAddressValue;
    EditText edtRealFloorValue;
    EditText edtRealAltitudeValue;

    double lastLatitude;
    double lastLongitude;

    double bfrLatitude;
    double bfrLongitude;
    int bfrAltitude;
    int bfrElevation;
    double bfrActualPressure;
    double bfrBasePressure;
    double bfrLocalBasePressure;
    String RealFloorValue;
    String RealAltitudeValue;
    private double precision=-1;

    boolean inBuilding = false;

    View view;
    Handler timeHandler = new Handler();
    int delay = 1000; //milliseconds
    Date time;
    SimpleDateFormat timeFormat;

    int buildingID;

    private String filterProData = "prodata";
    private String filterFloor = "floor";
    private String filterUpdate = "wantupdate";
    private String filterCountdown = "countdown";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.building_insert, container, false);
        btnBuildingEntrance = (Button)view.findViewById(R.id.buttonBuildingEntrance);
        btnBuildingFloor  = (Button)view.findViewById(R.id.buttonFloor);

        edtNameValue = (EditText)view.findViewById(R.id.editNameValue);
        edtAddressValue = (EditText)view.findViewById(R.id.editAddressValue);
        edtRealFloorValue = (EditText)view.findViewById(R.id.editRealFloorValue);
        edtRealAltitudeValue = (EditText)view.findViewById(R.id.editRealAltitudeValue);


        //imposto formattazione della data
        timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ITALY);

        //richiedo al servizio un update di coordinate, altitudine ecc...
        IntentFilter filter = new IntentFilter(filterProData);
        filter.addAction(filterCountdown);
        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mMessageReceiver, filter);
        requestUpdate();
        //inizializzo orologio
        timeHandler.postDelayed(new Runnable() {
            public void run() {

                time = Calendar.getInstance().getTime();
                //Log.d("TIME","dovrei essere chiamato ogni secondo!"+time);

                if (inBuilding) {
                    updateText(R.id.textTimeRilValue, timeFormat.format(time.getTime()));

                } else {
                    updateText(R.id.textTimeEnterValue, timeFormat.format(time.getTime()));
                    updateText(R.id.textTimeRilValue, timeFormat.format(time.getTime()));
                }
                timeHandler.postDelayed(this, delay);
            }
        }, delay);

        //quando premo il pulsante di entrata/uscita dei palazzi se...
        btnBuildingEntrance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String NameValue;
                String AddressValue;
                NameValue = edtNameValue.getText().toString();
                AddressValue = edtAddressValue.getText().toString();
                Intent logService = new Intent(getActivity(), logService.class);
                //... i campi non sono vuoti ...
                if (!NameValue.isEmpty() && !AddressValue.isEmpty()) {
                    //... e sono dentro un palazzo...
                    if (inBuilding) {

                        getActivity().stopService(logService);

                        //... significa che sto uscendo, quindi aggiorno voce del palazzo con tempo e pressione e pulisco i campi
                        edtNameValue.setText("");
                        edtAddressValue.setText("");
                        edtRealFloorValue.setText("");
                        edtRealAltitudeValue.setText("");

                        //switchInput(inBuilding);
                        btnBuildingEntrance.setText("Entro");
                    } else {
                        //... altrimenti sono entrato e credo la voce del palazzo
                        btnBuildingEntrance.setText("Esco");
                        Log.d("BUTTON_BUILDING", "pressione:" + bfrBasePressure);

                        logService.putExtra("name", NameValue);
                        logService.putExtra("address", AddressValue);
                        //logService.putExtra("latitude", lastLatitude);
                        logService.putExtra("latitude", bfrLatitude);
                        //logService.putExtra("longitude", lastLongitude);
                        logService.putExtra("longitude", bfrLongitude);
                        logService.putExtra("pressure", bfrActualPressure);
                        logService.putExtra("pressureaccuracy", precision);
                        logService.putExtra("basepressure", bfrBasePressure);
                        getActivity().startService(logService);
                    }
                    //attivo/disattivo pulsanti e campi relativi per impedire immissioni accidentali
                    switchInput(inBuilding);
                }else{
                    Toast.makeText(getActivity(), "Inserisci Nome ed Indirizzo!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        //se premo il pulsante per i piani...
        btnBuildingFloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RealFloorValue = edtRealFloorValue.getText().toString();
                RealAltitudeValue = edtRealAltitudeValue.getText().toString();

                //... e i campi non sono vuoti...
                if (!RealFloorValue.isEmpty() && !RealAltitudeValue.isEmpty()) {
                    //... aggiungo piano al db assicurandomi di collegarlo ad un palazzo
                    Log.d("CREATEFLOOR","altitude: "+String.valueOf(bfrAltitude));
                    edtRealFloorValue.setText("");
                    edtRealAltitudeValue.setText("");
                    sendFloorInfo();
                }else{
                    Toast.makeText(getActivity(), "Inserisci il Piano e l'Altitudine!",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    //attivo/disattivo elementi del layout in base allo stato dell'inserimento
    private void switchInput(boolean buildingEntered){
        btnBuildingFloor.setEnabled(!buildingEntered);

        edtNameValue.setEnabled(buildingEntered);
        edtAddressValue.setEnabled(buildingEntered);
        edtRealFloorValue.setEnabled(!buildingEntered);
        edtRealAltitudeValue.setEnabled(!buildingEntered);

        inBuilding = !buildingEntered;
    }

    //ricevitore per localbroadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(filterProData)){
                /*
                bfrLatitude = intent.getDoubleExtra("latitude",0);
                bfrLongitude = intent.getDoubleExtra("longitude", 0);

                bfrElevation = intent.getIntExtra("elevation", 0);
                bfrAltitude = intent.getIntExtra("altitudedelta", 0);
                bfrActualPressure = intent.getDoubleExtra("actualpressure", -1);
                bfrBasePressure = intent.getDoubleExtra("basepressure",-1);
                precision = intent.getDoubleExtra("sensorprec",-1);
                */
                readFromProvider();
                updateGUI();
            }else{
                if (intent.getAction().equals(filterCountdown)){
                    int countdown;
                    countdown = intent.getIntExtra("value",0);
                    if (countdown == 0){
                        btnBuildingFloor.setEnabled(true);
                        btnBuildingFloor.setText(getResources().getString(R.string.buttonChangeFloorText));
                    }else{
                        btnBuildingFloor.setEnabled(false);
                        btnBuildingFloor.setText(getResources().getString(R.string.buttonTransitFloorText)+" "+countdown);
                    }
                }
            }
        }
    };
    private void readFromProvider(){
        String URL_GPS = EspielProvider.GPS_URL;
        String URL_SENSOR = EspielProvider.USABLE_SENSOR_URL;

        Uri gps_data = Uri.parse(URL_GPS);
        Uri sensor_data = Uri.parse(URL_SENSOR);

        Cursor c_gps = getActivity().getBaseContext().getContentResolver().query(gps_data, null, null, null, null);
        Cursor c_sensor = getActivity().getBaseContext().getContentResolver().query(sensor_data,null,null,null,null);
        if (c_gps != null && c_gps.moveToFirst()){
            Log.d("TestProviderInsert","c_gps");
            bfrElevation = c_gps.getInt(0);
            bfrLatitude = c_gps.getDouble(1);
            bfrLongitude = c_gps.getDouble(2);
            c_gps.close();
        }
        if (c_sensor != null && c_sensor.moveToFirst()){
            Log.d("TestProviderInsert","c_sensor");
            bfrAltitude = c_sensor.getInt(2);
            bfrActualPressure = c_sensor.getDouble(1);
            bfrBasePressure = c_sensor.getDouble(0);
            precision = c_sensor.getDouble(4);
            c_sensor.close();
        }
        Log.d("TestProviderInsert","bfrElevation:" + bfrElevation+" bfrLatitude:"+bfrLatitude+
                " bfrLongitude:"+bfrLongitude+ " bfrAltitude:"+bfrAltitude+
                " bfrActualPressure:"+bfrActualPressure+" bfrBasePressure:"+bfrBasePressure+
                " precision:"+precision);
    }

    //ausiliaria per aggiornare l'interfaccia utente
    private void updateGUI() {
        if (bfrLatitude != positionService.NO_COORDINATE && bfrLongitude != positionService.NO_COORDINATE){
            lastLatitude = bfrLatitude;
            lastLongitude = bfrLongitude;
        }

        if (lastLatitude != positionService.NO_COORDINATE && lastLongitude != positionService.NO_COORDINATE)
            updateText(R.id.textCoordinateValue, String.valueOf(lastLatitude)+" "+String.valueOf(lastLongitude));
        if (bfrElevation != positionService.NO_COORDINATE)
            updateText(R.id.textElevationValue, String.valueOf(bfrElevation)+getResources().getString(R.string.SeaLevel));


        if (bfrAltitude != 0){
            updateText(R.id.textCalcAltitudeValue, String.valueOf(bfrAltitude));
            updateText(R.id.textCalcFloorValue, String.valueOf(bfrAltitude/3)+getResources().getString(R.string.Grade));
        }else{
            updateText(R.id.textCalcAltitudeValue, "0");
            updateText(R.id.textCalcFloorValue, "0"+getResources().getString(R.string.Grade));
        }

        Log.d("GUI",bfrActualPressure+":"+bfrBasePressure);
        updateText(R.id.textPressureValue, String.valueOf(bfrActualPressure));
        updateText(R.id.textPBEnterValue, String.valueOf(bfrBasePressure));
    }

    //ausiliaria per modificare i campi
    private void updateText(int id, String text){
        TextView textView = (TextView) view.findViewById(id);
        textView.setText(text);
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        LocalBroadcastManager.getInstance(view.getContext()).unregisterReceiver(mMessageReceiver);
        if (inBuilding) {
            Intent logService = new Intent(getActivity(), logService.class);
            getActivity().stopService(logService);
        }
    }

    //chiedo al servizio di mandarmi un aggiornamento
    private void requestUpdate(){
        Log.d("wantupdate", "Mando aggiornamento");
        Intent intent = new Intent(filterUpdate);
        LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(intent);
    }

    //chiedo al servizio di mandarmi un aggiornamento
    private void sendFloorInfo(){
        Log.d("sendFloorInfo", "Mando informazioni piano");
        Intent intent = new Intent(filterFloor);
        intent.putExtra("realfloor", RealFloorValue);
        intent.putExtra("realaltitude", RealAltitudeValue);
        LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(intent);
    }
}
